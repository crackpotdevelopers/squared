﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ArrowAnimation : MonoBehaviour {
	Image[] allImages;

//	List<ArrowAnim> arrowAnimList;
	// Use this for initialization
	private void Init () {
		ArrowAnim[] allChildren = transform.GetComponentsInChildren<ArrowAnim>();
		allImages = new Image [allChildren.Length];
		for (int i = 0; i < allChildren.Length; i++) {
			allImages [i] = allChildren [i].GetComponent<Image> ();
		}
//		print (allChildren.Length);

//		print("L: " + allChildren.Length);
//		foreach (Transform trans in allChildren) {
//		
//		}
	}

	public void StartArrowAnimator(){
		Init ();
		StartCoroutine (ArrowAnimator ());
	}

	private IEnumerator ArrowAnimator(){
		while (true) {
			
			for (int i = 0; i < allImages.Length; i++) {
				allImages [i].enabled = false;
			}
			yield return new WaitForSeconds (0.5f);
			for (int i = 0; i < allImages.Length / 3; i++) {
				allImages [i * 3+2].enabled = true;
			}
			yield return new WaitForSeconds (0.5f);
			for (int i = 0; i < allImages.Length / 3; i++) {
				allImages [i * 3 + 1].enabled = true;
			}
			yield return new WaitForSeconds (0.5f);
			for (int i = 0; i < allImages.Length / 3; i++) {
				allImages [i * 3].enabled = true;
			}
			yield return new WaitForSeconds (0.5f);

		}

	}

	
//	// Update is called once per frame
//	void Update () {
//	
//	}
}
