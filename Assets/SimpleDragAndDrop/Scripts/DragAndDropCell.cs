﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using SimpleTween;

/// <summary>
/// Every item's cell must contain this script
/// </summary>
[RequireComponent (typeof(Image))]
public class DragAndDropCell : MonoBehaviour
{
	public int row = -1;

	public int col = -1;

	public int itemCount;

	public List<int> emptyItemTypes;

	//	private bool isDropLocked;

	public enum CellType
	{
		Swap,
		// Items will be swapped between cells
		DropOnly,
		// Item will be dropped into cell
		DragOnly,
		// Item will be dragged from this cell
		UnlimitedSource
		// Item will be cloned and dragged from this cell
	}

	public CellType cellType = CellType.Swap;
	// Special type of this cell

	//	public struct DropDescriptor                                            // Struct with info about item's drop event
	//	{
	//		public DragAndDropCell sourceCell;
	//		// From this cell item was dragged
	//		public DragAndDropCell destinationCell;
	//		// Into this cell item was dropped
	//		public DragAndDropItem[] itemList;
	//		// dropped item
	//	}

	public delegate void PlacementEvent (int row, int col);

	static public event PlacementEvent OnItemPlacement;
	static public event PlacementEvent OnItemRemoval;

	bool isCellSuitable;
	// Sprite color for filled cell

	void OnEnable ()
	{
		DragAndDropItem.OnItemDragStartEvent += OnAnyItemDragStart;         // Handle any item drag start
		DragAndDropItem.OnItemDragEndEvent += OnAnyItemDragEnd;             // Handle any item drag end
	}

	void OnDisable ()
	{
		DragAndDropItem.OnItemDragStartEvent -= OnAnyItemDragStart;
		DragAndDropItem.OnItemDragEndEvent -= OnAnyItemDragEnd;
	}

	void Start ()
	{
		Initialisation ();
	}

	public void Reset ()
	{
		foreach (DragAndDropItem item in transform.GetComponentsInChildren<DragAndDropItem>()) {
			item.Recycle ();
		}
		Initialisation ();
	}

	public void ChangeColorList ()
	{
		foreach (DragAndDropItem item in transform.GetComponentsInChildren<DragAndDropItem>()) {
			item.ColorOfTheSquare = item.ColorOfTheSquare;
		}
	}

	void Initialisation ()
	{
		itemCount = 0;
//		isDropLocked = false;
		if (CellType.DragOnly == cellType)
			isCellSuitable = false;
		else
			isCellSuitable = true;
		emptyItemTypes = new List<int> { 0, 1, 2 };
//		SetBackgroundState (GetComponentInChildren<DragAndDropItem> () == null ? false : true);
	}

	/// <summary>
	/// On any item drag start need to disable all items raycast for correct drop operation
	/// </summary>
	/// <param name="item"> dragged item </param>
	private void OnAnyItemDragStart (DragAndDropItem[] itemList)
	{

		DragAndDropItem[] myItemList = GetComponentsInChildren<DragAndDropItem> (); // Get item from current cell
		if (myItemList != null && myItemList.Length > 0) {
			foreach (DragAndDropItem myItem in myItemList) {
				myItem.MakeRaycast (false);                                      // Disable item's raycast for correct drop handling
				//print ("heyyy you");
				foreach (DragAndDropItem item in itemList) {
					if (myItem == item) {                                             // If item dragged from this cell
						// Check cell's type
						switch (cellType) {
						case CellType.DropOnly:
//							foreach (GameObject icon in DragAndDropItem.iconList) {
							for (int i = 0; i < DragAndDropItem.iconList.Length; i++) {
								// Buraya alternatif bir cozum lazim
								DragAndDropItem.iconList [i].SetActive (false); //GetComponent<Image>().enabled = false; 
								// Item will not be dropped
							}

//							DragAndDropItem.iconList [0].SetActive (false);
							break;
						case CellType.UnlimitedSource:
                        // Nothing to do
							break;
						default:
//							print ("Call MakeVisible(false)");
							item.MakeVisible (false);                            // Hide item in cell till dragging
//							SetBackgroundState (false);
							break;
						}
					}
				}
			}
		}
	}

	/// <summary>
	/// On any item drag end enable all items raycast
	/// </summary>
	/// <param name="item"> dragged item </param>
	private void OnAnyItemDragEnd (DragAndDropItem[] itemList)
	{
		DragAndDropItem[] myItemList = GetComponentsInChildren<DragAndDropItem> (); // Get item from current cell
		if (myItemList != null && myItemList.Length > 0) {

			foreach (DragAndDropItem myItem in myItemList) {
				foreach (DragAndDropItem item in itemList) {
					if (myItem == item) {
//						SetBackgroundState (true);
						if (!isCellSuitable) {
							SoundManager.PlaySfx2 (SoundManager.dropNegativeAudio);
							if (CellType.DragOnly != cellType)
								isCellSuitable = true;
						}
					}
				}
				myItem.MakeRaycast (true);                                       // Enable item's raycast
			}
		} 
	}

	public void OnDropJob ()
	{
		if (DragAndDropItem.iconList != null && DragAndDropItem.iconList.Length > 0 ) {
			if (DragAndDropItem.iconList [0] != null && DragAndDropItem.iconList [0].activeSelf) { //GetComponent<Image>().enabled == true) {                    // If icon inactive do not need to drop item in cell
				DragAndDropItem[] itemList = DragAndDropItem.draggedItemList;
				DragAndDropCell sourceCell = DragAndDropItem.sourceCell;
				isCellSuitable = false;

				if ((itemList != null && itemList.Length > 0) && (sourceCell!=null) && (sourceCell != this)) {
					CheckAvailability (itemList, out isCellSuitable);

					switch (sourceCell.cellType) {                            // Check source cell's type
					case CellType.UnlimitedSource:
						for (int i = 0; i < itemList.Length; i++) {
							//							foreach (DragAndDropItem item in itemList) {
							string itemName = itemList [i].name;
							itemList [i] = Instantiate (itemList [i]);                       // Clone item from source cell
							itemList [i].name = itemName;
						}
						break;
					default:
						break;
					}
					switch (cellType) {                                       // Check this cell's type
					case CellType.Swap:
						DragAndDropItem[] currentItemList = GetComponentsInChildren<DragAndDropItem> ();
						switch (sourceCell.cellType) {
						case CellType.Swap:
							SwapItems (sourceCell, this);            // Swap items between cells
							if (currentItemList != null && currentItemList.Length > 0) {
								
							}
							break;
						default:
							PlaceItem (itemList, isCellSuitable);             // Place dropped item in this cell
							break;
						}
						break;
					case CellType.DropOnly:
						PlaceItem (itemList, isCellSuitable);                     // Place dropped item in this cell
						if (isCellSuitable) {
							sourceCell.itemCount -= itemList.Length;
						}
						SoundManager.PlaySfx (SoundManager.dropAudio);
						break;
					default:
						break;
					}
				}

				if (itemList != null && itemList.Length > 0 && itemList [0].GetComponentInParent<DragAndDropCell> () == null) {   // If item have no cell after drop
					foreach (DragAndDropItem item in itemList) {
						item.MakeVisible (true);
						ObjectPool.Recycle (item.gameObject);                    
					}
				}
			}
		}
	}

	IEnumerator AfterItemPlacement ()
	{
		yield return new WaitForEndOfFrame ();//WaitForSeconds (0.2f);
		yield return new WaitForSeconds (0.01f);
		if (OnItemPlacement != null) {
			OnItemPlacement (row, col);                                             
		}
		yield return new WaitForSeconds (0.1f);
//		isDropLocked = false;
	}


	//	/// <summary>
	//	/// Item is dropped in this cell
	//	/// </summary>
	//	/// <param name="data"></param>
	//	public void OnDrop (PointerEventData data)
	//	{
	//		OnDropJob ();
	//
	//	}

	//	/// <summary>
	//	/// Change cell's sprite color on item put/remove
	//	/// </summary>
	//	/// <param name="condition"> true - filled, false - empty </param>
	//	private void SetBackgroundState (bool condition)
	//	{
	//		GetComponent<Image> ().color = condition ? full : empty;
	//	}


	private void DeleteTheItem (DragAndDropItem item, bool condition)
	{
//		item.OnEndDragJob ();


		if (!emptyItemTypes.Contains ((int)item.typeOfTheSquare)) {
			itemCount--;
			emptyItemTypes.Add ((int)item.typeOfTheSquare);
		} 
//		else {
//			Debug.Log ("Sorunlu Remove: " + row + "X" + col);
//		}


		// Buraya kontrol ekle


		//				print ("RemoveItem: Row - " + row + " Col - " + col + " EmptyItemCount - " + emptyItemTypes.Count + " ItemObj - " + item.gameObject.name);

		if (OnItemRemoval != null) {
			OnItemRemoval (row, col);                                             
		}
		if (condition) {
			item.ChangeScaleTween (new Vector3 (0.5f, 0.5f, 0.5f), 1, 0.2f);
		} else {
			item.ChangeScaleTween (new Vector3 (0.01f, 0.01f, 0.01f), 1.5f, 0.8f);
		}



	}




	/// <summary>
	/// Delete item from this cell
	/// </summary>
	public void RemoveItem (int colorID)
	{
//		List<int> itemsToBeAdded = new List<int> ();
//		List<int> itemsToBeDeleted = new List<int> ();
//		print ("RemoveItem: " + colorID);
		DragAndDropItem[] items = GetComponentsInChildren<DragAndDropItem> ();
		bool condition = false;
		if (itemCount >= 3) {
			condition = true;
			for (int i = 0; i < items.Length; i++) {
				if (items [i].ColorOfTheSquare != colorID) {
					condition = false;
					break;
				}
			}
		}
		 

		for (int i = 0; i < items.Length; i++) {
			if (items [i].ColorOfTheSquare == colorID && itemCount > 0) {
				GameManager.ScoreManager.ScoreOfTheMove++;
				DeleteTheItem (items [i], condition);
			} 
//			else {
//				itemsToBeAdded.Add (i);
//			}
		}

//		foreach (int index in itemsToBeAdded) {
//
//		}
//
//		foreach (int index in itemsToBeDeleted) {
//		}

//		foreach (DragAndDropItem item in GetComponentsInChildren<DragAndDropItem>()) {
//			
//			if (item.ColorOfTheSquare == colorID && itemCount > 0) {
//				
//
////				Destroy (item.gameObject);
//			}
//		}

//		SetBackgroundState (false);
	}


	public void CheckAvailability (DragAndDropItem[] itemList, out bool condition)
	{
		condition = true;
		bool condition2;
		foreach (DragAndDropItem item in itemList) {
			condition2 = false;
			foreach (int availableType in emptyItemTypes) {
				if ((int)item.typeOfTheSquare == availableType) {
					condition2 = true;
					break;
				}
			}
			condition = condition && condition2;
		}
	


//		foreach (DragAndDropItem item in GetComponentsInChildren<DragAndDropItem>()) {
//			foreach (DragAndDropItem item2 in itemList) {
//				if (item.typeOfTheSquare == item2.typeOfTheSquare) {
//					condition = false;
//					return;
//				}
//			}
//		}
	
	}

	/// <summary>
	/// Put new item in this cell
	/// </summary>
	/// <param name="itemObj"> New item's object with DragAndDropItem script </param>
	public void PlaceItem (DragAndDropItem[] itemList, bool isCellSuitable)
	{

//		RemoveItem ();   // Remove current item from this cell
		if (isCellSuitable) {
//			isDropLocked = true;

			foreach (DragAndDropItem itemTemp in itemList) {
				GameObject itemObj = itemTemp.gameObject;
			                                                     
				if (itemObj != null) {
//					print (itemObj.name);

//					itemCount++;
					if (emptyItemTypes.Remove ((int)itemTemp.typeOfTheSquare)) {
						itemCount++;
					} 
//					else if(row == 50){
//					}else {
//						Debug.Log ("Sorunlu Add: " + row + "X" + col);
//					}
						
//					print ("PlaceItem: Row - " + row + " Col - " + col + " EmptyItemCount - " + emptyItemTypes.Count + " ItemObj - " + itemObj.name);
					itemObj.transform.SetParent (transform, false);
					itemObj.transform.localPosition = Vector3.zero;
//					itemObj.GetComponent<RectTransform>().position = Vector3.zero;

//					DragAndDropItem item = itemObj.GetComponent<DragAndDropItem> ();
					if (itemTemp != null) {
						itemTemp.MakeRaycast (true);
					}
//					SetBackgroundState (true);
				}
			}

//			if (row != 50) {
//				string s = "";
//				foreach (int id in emptyItemTypes) {
//					s += id + ",";
//				}
//				Debug.Log ("-- " + row + "X" + col + "(O):" + s + " -- ItemCount: " + itemCount);
//			}

			if (cellType == CellType.DropOnly) {
//				print (transform.parent.name);


				if (DragAndDropItem.iconList != null && DragAndDropItem.iconList.Length > 0) {
					foreach (GameObject icon in DragAndDropItem.iconList) {
//						icon.gameObject.SetActive (false);
						ObjectPool.Recycle (icon);                                                   
					}
				}
				transform.root.GetComponentInChildren<GameLogic> ().CheckTheMove (row, col);

				StartCoroutine (AfterItemPlacement ());
				//transform.parent.parent.parent.parent.GetComponent<GameLogic> ().CheckTheMove (row, col);
			}
		} 

	}

	/// <summary>
	/// Get item from this cell
	/// </summary>
	/// <returns> Item </returns>
	public DragAndDropItem GetItem ()
	{
		return GetComponentInChildren<DragAndDropItem> ();
	}

	/// <summary>
	/// Swap items between to cells
	/// </summary>
	/// <param name="firstCell"> Cell </param>
	/// <param name="secondCell"> Cell </param>
	public void SwapItems (DragAndDropCell firstCell, DragAndDropCell secondCell)
	{
		if ((firstCell != null) && (secondCell != null)) {
			DragAndDropItem firstItem = firstCell.GetItem ();                // Get item from first cell
			DragAndDropItem secondItem = secondCell.GetItem ();              // Get item from second cell
			if (firstItem != null) {
				// Place first item into second cell
				firstItem.transform.SetParent (secondCell.transform, false);
				firstItem.transform.localPosition = Vector3.zero;
//				secondCell.SetBackgroundState (true);
			}
			if (secondItem != null) {
				// Place second item into first cell
				secondItem.transform.SetParent (firstCell.transform, false);
				secondItem.transform.localPosition = Vector3.zero;
//				firstCell.SetBackgroundState (true);
			}
		}
	}

//	private IEnumerator NotifyOnDragEnd (DropDescriptor desc)
//	{
//		// Wait end of drag operation
//		while (DragAndDropItem.draggedItemList != null && DragAndDropItem.draggedItemList.Length > 0) {
//			yield return new WaitForEndOfFrame ();
//		}
//		// Send message with DragAndDrop info to parents GameObjects
//		gameObject.SendMessageUpwards ("OnItemPlace", desc, SendMessageOptions.DontRequireReceiver);
//	}
}
