﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using SimpleTween;

/// <summary>
/// Every "drag and drop" item must contain this script
/// </summary>
[RequireComponent (typeof(Image))]
public class DragAndDropItem : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{


	int colorOfTheSquare;
	public SquareType typeOfTheSquare;
	private Image image;

	//	public GameObject iconObject;

	// Square Type
	public enum SquareType
	{
		Small,
		Medium,
		Large

	}

	public int ColorOfTheSquare {
		get {
			return colorOfTheSquare;
		}
		set {
//			if (transform.parent != null)
//				print ("Color Change - " + gameObject.name + " Parent - " + transform.parent.gameObject.name);
			colorOfTheSquare = value;
			image.color = GameManager.ColorList.GetColorList () [colorOfTheSquare];
		}
	}

	static public DragAndDropItem[] draggedItemList;
	// Item that is dragged now
	static public GameObject[] iconList;
	// Icon of dragged item
	static public DragAndDropCell sourceCell;
	// From this cell dragged item is

	public delegate void DragEvent (DragAndDropItem[] item);

	static public event DragEvent OnItemDragStartEvent;
	// Drag start event
	static public event DragEvent OnItemDragEndEvent;
	// Drag end event

	//	public delegate void AnyActionEvent (bool condition);
	//
	//	static public event AnyActionEvent OnButtonClick;
	//	static public event AnyActionEvent OnButtonRelease;

	Canvas canvas;

	void Awake ()
	{
		image = transform.Find ("Image").GetComponent<Image> ();
	}

	void OnEnable ()
	{
		GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
		MakeRaycast (true);

	}

	void OnDisable ()
	{
		MakeRaycast (false);
	}


	//	void OnEnable(){
	//
	//	}
	//
	//	void OnDisable(){
	////		draggedItemList = null;
	////		iconList = null;
	////		sourceCell = null;
	////		OnItemDragEndEvent = null;
	////		OnItemDragStartEvent = null;
	//	}

	/// <summary>
	/// This item is dragged
	/// </summary>
	/// <param name="eventData"></param>
	//	public void OnBeginDrag (PointerEventData eventData)
	//	{
	//		if (!GameManager.isDragStarted) {
	//			GameManager.isDragStarted = true;
	//			sourceCell = GetComponentInParent<DragAndDropCell> ();                       // Remember source cell
	//			draggedItemList = transform.parent.GetComponentsInChildren<DragAndDropItem> ();   // Set as dragged item
	//			iconList = new GameObject[draggedItemList.Length];
	//			Vector2 pos;
	//			for (int i = 0; i < iconList.Length; i++) {
	//				iconList [i] = ObjectPool.Spawn (GameManager.instance.iconObject);                                              // Create object for item's icon
	//				Image image = iconList [i].GetComponent<Image> ();
	//				image.sprite = draggedItemList [i].image.sprite;
	//				image.color = draggedItemList [i].image.color;
	//				image.raycastTarget = false;                                                // Disable icon's raycast for correct drop handling
	//				RectTransform iconRect = iconList [i].GetComponent<RectTransform> ();
	//
	//				canvas = GetComponentInParent<Canvas> ();                             // Get parent canvas
	////				Transform parentTransform = transform.root.Find("IconHolder");
	//				if (canvas != null) {
	//					// Display on top of all GUI (in parent canvas)
	//					//CREATE ICON in the LevleMenu, better change later for a different game... 
	//
	//					iconList [i].transform.SetParent (canvas.transform.Find ("LevelMenu"), false);  
	//					iconList [i].transform.SetAsLastSibling ();                       // Set canvas as parent
	//					// Set as last child in canvas transform
	//				}
	//
	//				// Set icon's dimensions
	//				iconRect.sizeDelta = new Vector2 (draggedItemList [i].image.GetComponent<RectTransform> ().sizeDelta.x, draggedItemList [i].image.GetComponent<RectTransform> ().sizeDelta.y);
	//				//iconRect.sizeDelta = Vector2.one;
	//
	////				RectTransformUtility.ScreenPointToLocalPointInRectangle (canvas.transform as RectTransform, ScalePosition (Input.mousePosition), canvas.worldCamera, out pos);
	////				iconList [i].transform.position = canvas.transform.TransformPoint (pos);
	//
	//
	//
	//			}
	//			if (OnItemDragStartEvent != null) {
	//				OnItemDragStartEvent (transform.parent.GetComponentsInChildren<DragAndDropItem> ());                                             // Notify all about item drag start
	//			}
	//		}
	//	}

	public void OnPointerDown (PointerEventData eventData)
	{

		if (!GameManager.IsDragStarted) {
			GameManager.IsDragStarted = true;
			sourceCell = GetComponentInParent<DragAndDropCell> ();                       // Remember source cell
			draggedItemList = transform.parent.GetComponentsInChildren<DragAndDropItem> ();   // Set as dragged item
			iconList = new GameObject[draggedItemList.Length];
			Vector2 pos;
//			draggedItemList[0].transform.Find ("Eyes").gameObject.SetActive (false);
			for (int i = 0; i < iconList.Length; i++) {
				iconList [i] = ObjectPool.Spawn (GameManager.instance.iconObject);                                              // Create object for item's icon
				draggedItemList [i].GetComponent<RectTransform> ().anchoredPosition = new Vector3 (0, 0, 0);
				Image image = iconList [i].GetComponent<Image> ();
				image.sprite = draggedItemList [i].image.sprite;
				image.color = draggedItemList [i].image.color;
				image.raycastTarget = false;                                                // Disable icon's raycast for correct drop handling
				RectTransform iconRect = iconList [i].GetComponent<RectTransform> ();

				canvas = GetComponentInParent<Canvas> ();                             // Get parent canvas
				//				Transform parentTransform = transform.root.Find("IconHolder");
				if (canvas != null) {
					// Display on top of all GUI (in parent canvas)
					//CREATE ICON in the LevleMenu, better change later for a different game... 

					iconList [i].transform.SetParent (canvas.transform.Find ("LevelMenu"), false);  
					iconList [i].transform.SetAsLastSibling ();                       // Set canvas as parent
					// Set as last child in canvas transform
				}

				// Set icon's dimensions
				iconRect.sizeDelta = new Vector2 (draggedItemList [i].image.GetComponent<RectTransform> ().sizeDelta.x, draggedItemList [i].image.GetComponent<RectTransform> ().sizeDelta.y);
				//iconRect.sizeDelta = Vector2.one;

				RectTransformUtility.ScreenPointToLocalPointInRectangle (canvas.transform as RectTransform, ScalePosition (eventData.position), canvas.worldCamera, out pos);
				iconList [i].transform.position = canvas.transform.TransformPoint (pos);



			}
			if (OnItemDragStartEvent != null) {
				OnItemDragStartEvent (transform.parent.GetComponentsInChildren<DragAndDropItem> ());                                             // Notify all about item drag start
			}
		}
	}

	public void OnPointerUp (PointerEventData eventData)
	{
		StartCoroutine (PointerUpEnum ());
	}

	IEnumerator PointerUpEnum ()
	{
		yield return new WaitForEndOfFrame ();
//		yield return new WaitForEndOfFrame ();

		OnEndDragJob ();
		GameManager.IsDragStarted = false;
		 
//		yield return new WaitForEndOfFrame ();
	}

	/// <summary>
	/// Every frame on this item drag
	/// </summary>
	/// <param name="data"></param>
	public void OnDrag (PointerEventData data)
	{
		if (iconList != null && canvas != null && iconList.Length > 0) {
			Vector2 pos;
			for (int i = 0; i < iconList.Length; i++) {
				RectTransformUtility.ScreenPointToLocalPointInRectangle (canvas.transform as RectTransform, ScalePosition (data.position), canvas.worldCamera, out pos);
				iconList [i].transform.position = canvas.transform.TransformPoint (pos);

			}
		}
	}

	public Vector3 ScalePosition (Vector3 position)
	{
		//Vector2 ActualPosition;
//		RectTransform CanvasTransform = canvas.transform as RectTransform;


		Vector2 canvasSize = canvas.GetComponent<RectTransform> ().sizeDelta;
		Vector2 canvasSizeScaled = canvas.GetComponent<CanvasScaler> ().referenceResolution;
		Vector2 scale = new Vector2 (canvasSizeScaled.x / canvasSize.x, Screen.width / canvasSizeScaled.x);

//		Vector3 wantedPos = 
		//Vector3 TargetPosition = Position + new Vector2 (0, ((ContainerSizeScaled.y + CardOverlaySizeScaled.y) / 2 + 10 * Scale.y));

		//RectTransformUtility.ScreenPointToLocalPointInRectangle (CanvasTransform, TargetPosition, canvas.worldCamera, out ActualPosition);

		//Vector3 TransformedPosition = CanvasTransform.TransformPoint (ActualPosition);

		return new Vector3 (position.x, position.y + 100 * scale.y, position.z);
	}

	//	/// <summary>
	//	/// This item is dropped
	//	/// </summary>
	//	/// <param name="eventData"></param>
	//	public void OnEndDrag (PointerEventData eventData)
	//	{
	//		OnEndDragJob ();
	//		GameManager.isDragStarted = false;
	//	}

	public void OnEndDragJob ()
	{
		if (iconList != null && iconList.Length > 0) {
			foreach (GameObject icon in iconList) {
				ObjectPool.Recycle (icon);
//				Destroy (icon);                                                          // Destroy icon on item drop

			}
		}

//		print ("DraggedItemList: " + draggedItemList.Length);
		if (draggedItemList != null) {
			foreach (DragAndDropItem item in draggedItemList) {
//				print ("Call MakeVisible(true)");
				item.MakeVisible (true);
			}
		}
		//		MakeVisible (true);                                                          // Make item visible in cell
		if (OnItemDragEndEvent != null) {
			OnItemDragEndEvent (transform.parent.GetComponentsInChildren<DragAndDropItem> ());                                               // Notify all cells about item drag end
		}
		draggedItemList = null;
		iconList = null;
		sourceCell = null;
	
	}

	/// <summary>
	/// Enable item's raycast
	/// </summary>
	/// <param name="condition"> true - enable, false - disable </param>
	public void MakeRaycast (bool condition)
	{
		Image imageForRaycast = GetComponent<Image> ();
		if (imageForRaycast != null) {
			imageForRaycast.raycastTarget = condition;
		}
	}

	/// <summary>
	/// Enable item's visibility
	/// </summary>
	/// <param name="condition"> true - enable, false - disable </param>
	public void MakeVisible (bool condition)
	{
//		print ("MakeVisible: " + condition);
		image.enabled = condition;
	}

	// Animation for the creation of random drag cell items
	public void ChangeScaleTween (Vector3 difference, float multiply, float time)
	{


		//		Tween[] tweens = new Tween[itemsToChange.Length];
		//		for (int i = 0; i < itemsToChange.Length; i++) {

		//			DragAndDropItem item = itemsToChange [i];
		//		Vector3 gap = new item.transform.localPosition;

		transform.localScale *= multiply;
		//			print(time);
		transform.SetParent (transform.parent.parent);// SetParent (transform.parent);
		SimpleTweener.AddTween (() => transform.localScale, x => transform.localScale = x, difference, time).Ease (Easing.EaseIn).OnCompleted (() => {
			//							print("bitti");
			MakeVisible (true);
			ObjectPool.Recycle (gameObject);
		});
		//		}

	}
}
