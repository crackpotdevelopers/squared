﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//using System.Linq;
using UnityEngine.UI;
using SimpleTween;
using I2.Loc;


public class GameLogic : MonoBehaviour
{

	private class AnimationType
	{

		// 0 RowLine, 1 ColLine, 2 leftTopToRightBottomLine, 3 RightTopToLeftBottomLine, 4 stack
		public int type;

		// Animation Color
		public int colorID;

		public AnimationType (int type, int colorID)
		{
			this.type = type;
			this.colorID = colorID;
		}
	}

	// Score Text Fields
	private Text currentScoreText;
	private GameObject scoreMultiplierContainer;
	private ComboTextManager comboTextManager;
	private Text scoreMultText;
	private Text bestScoreText;
	private Text tutorialText;
	ArrowAnimation arrowAnimation;
	private int tutorialTurnCount = -1;
	private int fontSizeScoreMult;

	private Stack<Tween> tweenStack = new Stack<Tween> ();
	//	private Tween[] currentTweens;

	// Stack For Animation
	private Stack<AnimationType> animationStack = new Stack<AnimationType> ();

	// DragOnly Cells
	DragAndDropCell[] squareDragOnlyCells;

	// How Many Drag Cell are empty
	private int emptyDragCellCount;


	private IEnumerator instanceIEnum;

	// New Squares to instantiate; 0 - small, 1-medium, 2-large
	public GameObject[] instantiateObjectsFrom;

	// Available spaces that used for instantiation of a new square
	List<int>[,] emptyItemTypes;

	Stack<int> tutorialStackIdList;

	// Random used for shuffle
	private System.Random randomForShuffle = new System.Random ();

	// sizeIDS; 0 - small, 1-medium, 2-large
	private int[] sizeIDArray = { 0, 1, 2 };

	// DropOnly Cells
	DragAndDropCell[,] boardCells;

	// RowCount and ColCount of the board
	private int rowCount = 3;
	private int colCount = 3;

	// Stack for items that need to be removed after each move
	private Stack<ItemRemover> deletionStack = new Stack<ItemRemover> ();


	void OnEnable ()
	{
		// OnItemPlacement and OnItemRemoval Actions of DragAndDropCell
		DragAndDropCell.OnItemPlacement += AfterPlacementOfThree;         
		DragAndDropCell.OnItemRemoval += AfterRemoval;
		DragAndDropItem.OnItemDragStartEvent += PointerDown;
		DragAndDropItem.OnItemDragEndEvent += PointerUp;
	}

	void PointerDown (DragAndDropItem[] itemList)
	{

		while (tweenStack.Count > 0) {
			SimpleTweener.RemoveTween (tweenStack.Pop ());

		}
//		for (int i = 0; i < itemList.Length; i++) {
//			itemList [i].GetComponent<RectTransform> ().anchoredPosition = new Vector3 (0, 0, 0);
//			itemList [i].transform.Find ("Eyes").gameObject.SetActive (false);
//		}
//		squareDragOnlyCells [random].transform.GetChild (i).Find ("Eyes").gameObject.SetActive (true);

//		currentTweens = null;
		if (instanceIEnum != null) {
			StopCoroutine (instanceIEnum);
		}

	}

	void PointerUp (DragAndDropItem[] itemList)
	{
//		print ("burda2");
		if (instanceIEnum != null) {
			StopCoroutine (instanceIEnum);
		}
		instanceIEnum = WaitForAnyMove (8.0f);
		StartCoroutine (instanceIEnum);
	}

	void AnimationForWaiting (float time)
	{
		if (emptyDragCellCount < 3) {
			int random = -1;
			if (emptyDragCellCount == 0) {
				random = Random.Range (0, squareDragOnlyCells.Length);
			} else {
				random = Random.Range (0, squareDragOnlyCells.Length);
				if (squareDragOnlyCells [random].transform.childCount <= 0) {
					for (int i = 0; i < squareDragOnlyCells.Length; i++) {
						if (random != i && squareDragOnlyCells [i].transform.childCount > 0) {
							random = i;
							break;
						}
					}
				}
//				random = -1;
			}
			if (random >= 0) {
//				GameObject eyeObject = null;
				for (int i = 0; i < squareDragOnlyCells [random].transform.childCount; i++) {
					
					RectTransform rectTransform = squareDragOnlyCells [random].transform.GetChild (i).GetComponent<RectTransform> ();
//					if (i == 0) {
//						eyeObject = rectTransform.transform.Find ("Eyes").gameObject;
//						eyeObject.SetActive (true);
//					}
					Vector3 pos = rectTransform.position;
					Tween tweenToUse = null;
					tweenToUse = SimpleTweener.AddTween (() => rectTransform.position, x => rectTransform.position = x, rectTransform.position + new Vector3 (0, 1, 0), time).Ease (Easing.EaseOut).OnCompleted (() => {
						tweenToUse = SimpleTweener.AddTween (() => rectTransform.position, x => rectTransform.position = x, pos, time).Ease (Easing.EaseOut).OnCompleted (() => {
							tweenToUse = SimpleTweener.AddTween (() => rectTransform.position, x => rectTransform.position = x, rectTransform.position + new Vector3 (0, 0.5f, 0), time).Ease (Easing.EaseOut).OnCompleted (() => {
								tweenToUse = SimpleTweener.AddTween (() => rectTransform.position, x => rectTransform.position = x, pos, time).Ease (Easing.EaseOut).OnCompleted (() => {
//									eyeObject.SetActive (false);
								});
								tweenStack.Push (tweenToUse);
							});
							tweenStack.Push (tweenToUse);
						});
						tweenStack.Push (tweenToUse);
					});
					tweenStack.Push (tweenToUse);


				}
			}
		}

	}

	IEnumerator WaitForAnyMove (float time)
	{
//		yield return new WaitForSeconds (5.0f);
//		print("burda3");
		while (true) {
			while (tweenStack.Count > 0) {
				tweenStack.Pop ();
			}
			yield return new WaitForSeconds (time / 2);
			AnimationForWaiting (0.1f);
			yield return new WaitForSeconds (time / 2);

		}
	
	}

	public void RestartTheGame ()
	{
		
//		Debug.Log ("\n\n----- GAME START -----\n\n");
		GameManager.ScoreManager.Reset ();
		if (boardCells [0, 0] != null) {
			foreach (DragAndDropCell cell in boardCells) {
				cell.Reset ();
			}
		}
		if (squareDragOnlyCells [0] != null) {
			foreach (DragAndDropCell cell in squareDragOnlyCells) {
				cell.Reset ();
			}
		}


		DragAndDropIcon[] dragAndDropIcons = transform.root.GetComponentsInChildren<DragAndDropIcon> ();

		foreach (DragAndDropIcon icon in dragAndDropIcons) {
			icon.Recycle ();
		}

	

		Initialisation ();

	}


	public void ChangeColors ()
	{
		if (boardCells != null && boardCells [0, 0] != null && squareDragOnlyCells != null && squareDragOnlyCells [0] != null) {
			foreach (DragAndDropCell cell in boardCells) {
				cell.ChangeColorList ();
//				print ("aaa");
			}
			foreach (DragAndDropCell cell in squareDragOnlyCells) {
				cell.ChangeColorList ();
			}
		}
	}


	void OnDisable ()
	{
		// OnItemPlacement and OnItemRemoval Actions of DragAndDropCell
		DragAndDropCell.OnItemPlacement -= AfterPlacementOfThree;  
		DragAndDropCell.OnItemRemoval -= AfterRemoval;
	}

	public void Initialisation ()
	{

		comboTextManager = transform.Find ("TopMenu/ComboMenu").GetComponent<ComboTextManager> ();
	
		emptyDragCellCount = 0;

		// Set Score to 0 at the beginning
		//		GameManager.ScoreManager.ScoreCalculation (currentScoreText);
		currentScoreText.text = "0";

		// Deactivate Score Multiplier Text
		scoreMultiplierContainer.SetActive (false);
		//scoreMultText.gameObject.SetActive (false);

		// Get Highest Score to best score text field
		bestScoreText.text = Mathf.CeilToInt (SaveManager.GetLevelScore ("Level01")).ToString ();

		// Set Each BoardCell [row,col]
		// Set Each EmptyItemTypes[row,col] to {0,1,2} array
		for (int i = 0; i < rowCount * colCount; i++) {
			boardCells [i / colCount, i % colCount] = transform.Find ("DropOnly/Sheet").GetChild (i).GetComponentInChildren<DragAndDropCell> ();		
			boardCells [i / colCount, i % colCount].row = i / colCount;
			boardCells [i / colCount, i % colCount].col = i % colCount;
			emptyItemTypes [i / colCount, i % colCount] = new List<int> (){ 0, 1, 2 };
		}





		if (tutorialTurnCount == -1 && SaveManager.GetStartCount () < 5) {
			
			tutorialTurnCount = 0;
			tutorialText.transform.parent.gameObject.SetActive (true);
			tutorialText.text = ScriptLocalization.Get ("Tutorial Text1");
//			arrowAnimation.gameObject.SetActive (true);
//			arrowAnimation.StartArrowAnimator ();
			tutorialStackIdList = new Stack<int> ();
			for (int i = 0; i < 3; i++) {
				tutorialStackIdList.Push (i);
			}
			instanceIEnum = WaitForAnyMove (3.0f);
		} else {
			tutorialTurnCount = -2;
			tutorialStackIdList = null;
			instanceIEnum = WaitForAnyMove (8.0f);
		}
		StartCoroutine (instanceIEnum);

		int a;
		DragAndDropItem[] b;

		for (int i = 0; i < squareDragOnlyCells.Length; i++) {

			// Create Random Square items for the dragonly cell i
			squareDragOnlyCells[i].row = 50;
			squareDragOnlyCells [i].col = i;			
			AfterPlacement (squareDragOnlyCells [i], squareDragOnlyCells [i].row, squareDragOnlyCells [i].col, i * 0.25f + 0.3f, out b, out a);

		}
//		} else {
//			TutorialBegin ();
//		}

	}

	void TutorialBegin ()
	{
		int a;
		DragAndDropItem[] b;

		for (int i = 0; i < squareDragOnlyCells.Length; i++) {

			// Create Random Square items for the dragonly cell i
			AfterPlacement (squareDragOnlyCells [i], squareDragOnlyCells [i].row, squareDragOnlyCells [i].col, i * 0.25f + 0.3f, out b, out a);

		}
	}

	void Awake ()
	{
		// Find score Text Fields
		currentScoreText = transform.Find ("TopMenu/CurrentScoreText").GetComponent<Text> ();
		scoreMultiplierContainer = transform.Find ("TopMenu/ScoreMultiplierContainer").gameObject;
		scoreMultText = scoreMultiplierContainer.GetComponentInChildren<Text> ();
		bestScoreText = transform.Find ("TopMenu/BestScoreContainer/BestScoreText").GetComponent<Text> ();
		tutorialText = transform.Find ("TutorialMenu/TutorialText").GetComponent<Text> ();
		arrowAnimation = transform.Find ("ArrowMenu").GetComponent<ArrowAnimation> ();

		// BoardCells instantioation
		boardCells = new DragAndDropCell[rowCount, colCount];
		emptyItemTypes = new List<int>[rowCount, colCount];

		// Instantiate DragOnly Cells
		squareDragOnlyCells = transform.Find ("DragOnly/Sheet").GetComponentsInChildren<DragAndDropCell> ();
		fontSizeScoreMult = scoreMultText.fontSize;

	}

	// After Removal happened in each cell, this method is called
	void AfterRemoval (int row, int col)
	{
//		print ("Break_4 - time" + Time.realtimeSinceStartup);
//		Debug.Break();
		emptyItemTypes [row, col] = boardCells [row, col].emptyItemTypes;
	}

	// AfterPlacementOfThree Method is called after each item placement
	void AfterPlacementOfThree (int row, int col)
	{
//		print("Break_1 - time" + Time.realtimeSinceStartup);
//		Debug.Break();
		// If it is not the call of the method at the beginning
		if (row >= 0)
			emptyItemTypes [row, col] = boardCells [row, col].emptyItemTypes;

		// Call of part 1
		AfterPlacementOfThreePart1 ();

	}

	void AfterPlacementOfThreePart1 ()
	{

		for (int i = 0; i < rowCount * colCount; i++) {
			if (emptyItemTypes [i / colCount, i % colCount].Count > 0) {
				// If there is an empty Space move to part 2
				AfterPlacementOfThreePart2 ();
				return;
			} 
		}
//		print ("No Available Space Left");

		GameOverCall ();
	}

	void GameOverCall ()
	{
//		for (int i = 0; i < squareDragOnlyCells.Length; i++) {
//			if (squareDragOnlyCells [i].transform.childCount > 0) {
//				foreach (DragAndDropItem item in squareDragOnlyCells[i].GetComponentsInChildren<DragAndDropItem>()) {
//					item.Recycle ();
//				}
//			}
//		}
//		print("Break_2 - time" + Time.realtimeSinceStartup);
//		Debug.Break();
//		UnityEditor.EditorApplication.isPlaying = false;

//		transform.Find ("BlockRaycaster").gameObject.SetActive (true);
		// If there is no empty Space
		GameManager.instance.GameOverMethod ();
	}


	void AfterPlacementOfThreePart2 ()
	{
		// If more than 2 empty drag cells
		if (emptyDragCellCount >= 2) {


			// Create Added items array
			DragAndDropItem[][] addedItems = new DragAndDropItem[squareDragOnlyCells.Length][];

			// Create Random cell array
			int[] randomList = new int[squareDragOnlyCells.Length];

			for (int i = 0; i < squareDragOnlyCells.Length; i++) {
					
				// Create Random Square items for the dragonly cell i
				AfterPlacement (squareDragOnlyCells [i], squareDragOnlyCells [i].row, squareDragOnlyCells [i].col, i * 0.25f + 0.3f, out addedItems [i], out randomList [i]);

				if (addedItems [i] != null) {
					foreach (DragAndDropItem item in addedItems[i]) {
						// Place that created items into field to create the next one logically
						emptyItemTypes [randomList [i] / rowCount, randomList [i] % rowCount].Remove ((int)item.typeOfTheSquare);
					}
				}

			}

			// Delete placed items from the fields
			for (int i = 0; i < squareDragOnlyCells.Length; i++) {
				if (addedItems [i] != null) {
					foreach (DragAndDropItem item in addedItems[i]) {
						emptyItemTypes [randomList [i] / rowCount, randomList [i] % rowCount].Add ((int)item.typeOfTheSquare);
					}
				}
			}
			// Make empty drag cell count 0
			emptyDragCellCount = 0;
			if (tutorialTurnCount >= 1) {
				tutorialTurnCount++;
			}
		} else {

			// If there exist available space
			if (CheckPlaceAvailability ()) {
				emptyDragCellCount++;
			}
			// If there is no move left finish the game
			else {
//				print ("No suitable object match Left");
				GameOverCall ();
			}
		}
	}

	// Check suitability of drag items with the board cells
	bool CheckPlaceAvailability ()
	{
		bool condition1;
		// For each drag cell
		for (int i = 0; i < squareDragOnlyCells.Length; i++) {
			if (squareDragOnlyCells [i].transform.childCount > 0) {
				// For each cell of the board
				for (int j = 0; j < rowCount * colCount; j++) {
					// Check suitability of the row j/rowcount and col j%rowcount with drag cell i
					boardCells [j / rowCount, j % rowCount].CheckAvailability (squareDragOnlyCells [i].GetComponentsInChildren<DragAndDropItem> (), out condition1);

					// if they are suitable return true, because there exist a move
					if (condition1) {
						return true;
					} 
				} 

			}
		}

		// if there is no move left return false
		return false;
	}

	// Animation for the creation of random drag cell items
	void ChangeLocationTween (DragAndDropItem[] itemsToChange, Vector3 difference, float time)
	{
	

		Tween[] tweens = new Tween[itemsToChange.Length];
		for (int i = 0; i < itemsToChange.Length; i++) {

			DragAndDropItem item = itemsToChange [i];
			Vector3 gap = item.transform.localPosition;

			item.transform.localPosition += difference;
//			print(time);
			tweens [i] = SimpleTweener.AddTween (() => item.transform.localPosition, x => item.transform.localPosition = x, gap, time).Ease (Easing.EaseIn);
//				.OnCompleted (() => {
//				print("bitti");
//			});
		}

	}
		
	//	// After placement show version
	//	void AfterPlacement (DragAndDropCell sourceCell, int row, int col)
	//	{
	//		int a;
	//		DragAndDropItem[] b;
	//		AfterPlacement (sourceCell, row, col,0.01f, out  b, out  a);
	//	}

	// After item placement to each cell, this method is called
	void AfterPlacement (DragAndDropCell sourceCell, int row, int col, float animationTime, out DragAndDropItem[] randomSquareItems, out int random)
	{

		// Select Random cell from the board
		random = Random.Range (0, 9);

		int initialRandRow = random;
		bool endTheLoop = true;

		// while that cell is full, move to the next cell
		while (emptyItemTypes [random / rowCount, random % rowCount].Count <= 0 && endTheLoop) {
				
			random = (random + 1) % 9;
			// If there is no available row, end the game
			if (initialRandRow == random) {
				endTheLoop = false;
				if (emptyItemTypes [random / rowCount, random % rowCount].Count <= 0) {


					CreateNewSquareObjects (out randomSquareItems, sourceCell, animationTime, -1);

					randomSquareItems = null;
					return;
				}
			}
		}



		CreateNewSquareObjects (out randomSquareItems, sourceCell, animationTime, random);

//		// Set each of the randomSquareItems with random color and random square type
//		for (int j = 0; j < randomSquareItems.Length; j++) {
//			GameObject gameObject = ObjectPool.Spawn (instantiateObjectsFrom [emptyItemTypes [random / rowCount, random % rowCount] [j]]);                       // Clone item from source cell
//			randomSquareItems [j] = gameObject.GetComponent<DragAndDropItem> ();
//			randomSquareItems [j].ColorOfTheSquare = Random.Range (0, GameManager.ColorList.GetColorList ().Length);
//		
//		}
//
//		// Place The Randomly created items to dragonly cell
//		sourceCell.PlaceItem (randomSquareItems, true);
//		ChangeLocationTween (randomSquareItems, new Vector3 (0, -400, 0), animationTime);
	}

	void CreateNewSquareObjects (out DragAndDropItem[] randomSquareItems, DragAndDropCell sourceCell, float animationTime, int random)
	{
//		print (tutorialTurnCount);

		if (tutorialTurnCount < 0 || tutorialTurnCount >= 2) {
			int[] instantiationFrom;
			if (random >= 0) {
		
				// Create random squareItems array with the size of Random number from 1 to count of empty spaces of the cell 
				if (emptyItemTypes [random / rowCount, random % rowCount].Count > 1) {
					randomSquareItems = new DragAndDropItem[Random.Range (1, 3)];
				} else {
					randomSquareItems = new DragAndDropItem[1];
				}

				// Shuffle the empty item array of the cell
				Shuffle (emptyItemTypes [random / rowCount, random % rowCount]);

				instantiationFrom = emptyItemTypes [random / rowCount, random % rowCount].ToArray ();
			} else {
		
				// Create random squareItems array with the size of 1 or 2 or 3
				randomSquareItems = new DragAndDropItem[Random.Range (1, 3)];

				// Shuffle the SizeID Array
				Shuffle (sizeIDArray);

				instantiationFrom = sizeIDArray;
			}


			int randomColorLength;
			if (GameManager.ScoreManager.TotalScore <= 30) {
				randomColorLength = 3;
			} else if (GameManager.ScoreManager.TotalScore < GameManager.ColorList.GetColorList ().Length * 10 && GameManager.ScoreManager.TotalScore > 10) {
				randomColorLength = GameManager.ScoreManager.TotalScore / 10;
			} else {
				randomColorLength = GameManager.ColorList.GetColorList ().Length;
			}

			// Set each of the randomSquareItems with random color and random square type
			for (int j = 0; j < randomSquareItems.Length; j++) {
				GameObject gameObject = ObjectPool.Spawn (instantiateObjectsFrom [instantiationFrom [j]]);    
				randomSquareItems [j] = gameObject.GetComponent<DragAndDropItem> ();
				randomSquareItems [j].ColorOfTheSquare = Random.Range (0, randomColorLength);
			}


			sourceCell.PlaceItem (randomSquareItems, true);
			ChangeLocationTween (randomSquareItems, new Vector3 (0, -400, 0), animationTime);

//			if(randomSquareItems!=null && randomSquareItems.Length > 0)
//				randomSquareItems [0].transform.Find ("Eyes").gameObject.SetActive (true);
//
			if (tutorialTurnCount == 2) {
				tutorialText.text = ScriptLocalization.Get ("Tutorial Text2");
			} else if (tutorialTurnCount == 3) {
				tutorialText.text = ScriptLocalization.Get ("Tutorial Text3");
			} else if (tutorialTurnCount == 4) {
				tutorialText.transform.parent.gameObject.SetActive (false);
			}

		} else {
			switch (tutorialTurnCount) {
			case 0:
//			sizeIDArray = { 0, 1, 2 };
				randomSquareItems = new DragAndDropItem[1];
			// Set each of the randomSquareItems with random color and random square type
				for (int j = 0; j < randomSquareItems.Length; j++) {
					GameObject gameObject = ObjectPool.Spawn (instantiateObjectsFrom [0]);    
					randomSquareItems [j] = gameObject.GetComponent<DragAndDropItem> ();
					randomSquareItems [j].ColorOfTheSquare = 1;
				}

				sourceCell.PlaceItem (randomSquareItems, true);
				ChangeLocationTween (randomSquareItems, new Vector3 (0, -400, 0), animationTime);
				break;
			case 1:
				randomSquareItems = new DragAndDropItem[1];
				// Set each of the randomSquareItems with random color and random square type
				for (int j = 0; j < randomSquareItems.Length; j++) {
					GameObject gameObject = ObjectPool.Spawn (instantiateObjectsFrom [tutorialStackIdList.Pop ()]);    
					randomSquareItems [j] = gameObject.GetComponent<DragAndDropItem> ();
					randomSquareItems [j].ColorOfTheSquare = 2;
				}

				sourceCell.PlaceItem (randomSquareItems, true);
				ChangeLocationTween (randomSquareItems, new Vector3 (0, -400, 0), animationTime);
				tutorialText.text = ScriptLocalization.Get ("Tutorial Text4");
				break;
			default:
				randomSquareItems = null;
				break;
			}
			
		}
	}

	// Shuffle Method of Array
	public void Shuffle<T> (T[] array)
	{
		int n = array.Length;
		for (int i = 0; i < n; i++) {
			// NextDouble returns a random number between 0 and 1.
			// ... It is equivalent to Math.random() in Java.
			int r = i + (int)(randomForShuffle.NextDouble () * (n - i));
			T t = array [r];
			array [r] = array [i];
			array [i] = t;
		}
	}

	// Shuffle Method of List
	public void Shuffle<T> (List<T> array)
	{
		int n = array.Count;
		for (int i = 0; i < n; i++) {
			// NextDouble returns a random number between 0 and 1.
			// ... It is equivalent to Math.random() in Java.
			int r = i + (int)(randomForShuffle.NextDouble () * (n - i));
			T t = array [r];
			array [r] = array [i];
			array [i] = t;
		}
	}

	// Check Corner from left top to right bottom
	void CheckCornersRowEqualsCol (int row, int col, int colorID)
	{
		bool condition;
		if (colorID < 0) {
			DragAndDropItem[] items = boardCells [row, col].GetComponentsInChildren<DragAndDropItem> ();

			for (int i = 0; i < items.Length; i++) {
				condition = true;
				for (int j = 0; j < i; j++) {
					if (items [i].ColorOfTheSquare == items [j].ColorOfTheSquare) {
						condition = false;
					}
				}
				if (condition) {
					CheckCornersRowEqualsCol (row + 1, col + 1, items [i].ColorOfTheSquare);
				}
			}
				
		} else if (row >= rowCount) {

			for (int i = 0; i < rowCount; i++) {
				ItemRemover itemRemover = new ItemRemover (i, i, colorID);
				deletionStack.Push (itemRemover);

			}
			animationStack.Push (new AnimationType (2, colorID));

			return;
		} else {
			DragAndDropItem[] items = boardCells [row, col].GetComponentsInChildren<DragAndDropItem> ();

			for (int i = 0; i < items.Length; i++) {
				condition = true;
				for (int j = 0; j < i; j++) {
					if (items [i].ColorOfTheSquare == items [j].ColorOfTheSquare) {
						condition = false;
					}
				}
				if (condition && items [i].ColorOfTheSquare == colorID) {
					CheckCornersRowEqualsCol (row + 1, col + 1, colorID);
				}
			}
				
		}
	}

	void CheckCornersRowEqualsSizeMinusCol (int row, int col, int colorID)
	{
		bool condition;
		if (colorID < 0) {

			DragAndDropItem[] items = boardCells [row, col].GetComponentsInChildren<DragAndDropItem> ();

			for (int i = 0; i < items.Length; i++) {
				condition = true;
				for (int j = 0; j < i; j++) {
					if (items [i].ColorOfTheSquare == items [j].ColorOfTheSquare) {
						condition = false;
					}
				}
				if (condition) {
					CheckCornersRowEqualsSizeMinusCol (row + 1, col - 1, items [i].ColorOfTheSquare);
				}
			}
				
		} else if (row >= rowCount) {
			
			for (int i = 0; i < rowCount; i++) {
				ItemRemover itemRemover = new ItemRemover (i, rowCount - 1 - i, colorID);
				deletionStack.Push (itemRemover);

			}
			animationStack.Push (new AnimationType (3, colorID));

			return;
		} else {

			DragAndDropItem[] items = boardCells [row, col].GetComponentsInChildren<DragAndDropItem> ();

			for (int i = 0; i < items.Length; i++) {
				condition = true;
				for (int j = 0; j < i; j++) {
					if (items [i].ColorOfTheSquare == items [j].ColorOfTheSquare) {
						condition = false;
					}
				}
				if (condition && items [i].ColorOfTheSquare == colorID) {
					CheckCornersRowEqualsSizeMinusCol (row + 1, col - 1, colorID);
				}
			}
				
		}
	}

	void CheckCols (int row, int col, int colorID)
	{
//		print ("Row: " + row + "Col: " + col + "ColorID: " + colorID);

		bool condition;
		if (colorID < 0) {

			DragAndDropItem[] items = boardCells [row, col].GetComponentsInChildren<DragAndDropItem> ();
//			bool condition;
			for (int i = 0; i < items.Length; i++) {
				condition = true;
				for (int j = 0; j < i; j++) {
					if (items [i].ColorOfTheSquare == items [j].ColorOfTheSquare) {
						condition = false;
					}
				}
				if (condition) {
					CheckCols (row, col + 1, items [i].ColorOfTheSquare);
				}
			}
				
		} else if (col >= colCount) {
			
			for (int i = 0; i < rowCount; i++) {
				ItemRemover itemRemover = new ItemRemover (row, i, colorID);
				deletionStack.Push (itemRemover);
			}
			animationStack.Push (new AnimationType (1, colorID));

			return;
		} else {
			DragAndDropItem[] items = boardCells [row, col].GetComponentsInChildren<DragAndDropItem> ();

			for (int i = 0; i < items.Length; i++) {
				condition = true;
				for (int j = 0; j < i; j++) {
					if (items [i].ColorOfTheSquare == items [j].ColorOfTheSquare) {
						condition = false;
					}
				}
				if (condition && items [i].ColorOfTheSquare == colorID) {
					CheckCols (row, col + 1, colorID);
				}
			}
				
		}
	}

	void CheckRows (int row, int col, int colorID)
	{
		bool condition;
		if (colorID < 0) {
			DragAndDropItem[] items = boardCells [row, col].GetComponentsInChildren<DragAndDropItem> ();

			for (int i = 0; i < items.Length; i++) {
				condition = true;
				for (int j = 0; j < i; j++) {
					if (items [i].ColorOfTheSquare == items [j].ColorOfTheSquare) {
						condition = false;
					}
				}
				if (condition) {
					CheckRows (row + 1, col, items [i].ColorOfTheSquare);
				}
			}

		} else if (row >= rowCount) {

			for (int i = 0; i < rowCount; i++) {
				ItemRemover itemRemover = new ItemRemover (i, col, colorID);
				deletionStack.Push (itemRemover);
			}
			animationStack.Push (new AnimationType (0, colorID));
			return;
		} else {
			DragAndDropItem[] items = boardCells [row, col].GetComponentsInChildren<DragAndDropItem> ();

			for (int i = 0; i < items.Length; i++) {
				condition = true;
				for (int j = 0; j < i; j++) {
					if (items [i].ColorOfTheSquare == items [j].ColorOfTheSquare) {
						condition = false;
					}
				}
				if (condition && items [i].ColorOfTheSquare == colorID) {
					CheckRows (row + 1, col, colorID);
				}
			}

		}
	}


	public void CheckSelf (int row, int col)
	{
//		print (row + "," + col + "," + boardCells [row, col].transform.childCount);
		if (boardCells [row, col].transform.childCount >= 3) {
			DragAndDropItem[] items = boardCells [row, col].GetComponentsInChildren<DragAndDropItem> ();
			bool isAllColorsSame = true;
			int colorID = items [0].ColorOfTheSquare;
			foreach (DragAndDropItem item in items) {
				if (item.ColorOfTheSquare != colorID) {
					isAllColorsSame = false;
				}
			}
			if (isAllColorsSame) {
				ItemRemover itemRemover = new ItemRemover (row, col, colorID);
//				print ("burda");
				deletionStack.Push (itemRemover);
				animationStack.Push (new AnimationType (4, colorID));
			}

		} 


	}

	public void CheckTheMove (int row, int col)
	{

		// Sol ustten sag alta capraz
		if (row == col) {
			// Tam ortadaki ise
			if (row == (rowCount - 1) / 2) {
				CheckCornersRowEqualsSizeMinusCol (0, rowCount - 1, -1);
			}
			CheckCornersRowEqualsCol (0, 0, -1);

		}
		// Sag ustten sol alta capraz
		else if (row == rowCount - 1 - col) {
			CheckCornersRowEqualsSizeMinusCol (0, rowCount - 1, -1);
		}
			

		CheckRows (0, col, -1);
		CheckCols (row, 0, -1);
		CheckSelf (row, col);
		// Row degeri row olanlari ve
		// Col degeri col olanlari kontrol et
	
		if (tutorialTurnCount == 0 && deletionStack.Count > 0) {
			tutorialTurnCount++;
			tutorialText.text = ScriptLocalization.Get ("Tutorial Text5");
//			arrowAnimation.gameObject.SetActive (false);
		}
		StartCoroutine (DeleteEnum ());

		CallToAnimations (row, col);


	}

	IEnumerator DeleteEnum ()
	{
		yield return new WaitForEndOfFrame ();

//		print("Break_3 - time" + Time.realtimeSinceStartup);
//		Debug.Break();
		while (deletionStack.Count > 0) {
			
			ItemRemover itemRemover = deletionStack.Pop ();
//			print (itemRemover.row + "," + itemRemover.col + "," + itemRemover.colorID);
			boardCells [itemRemover.row, itemRemover.col].RemoveItem (itemRemover.colorID);
		}
		GameManager.ScoreManager.ScoreCalculation (currentScoreText);
		GameManager.ScoreManager.ScoreOfTheMove = 0;
//		PrintEmptyItemList ();
	}

	public void PrintEmptyItemList ()
	{
		string s = "-";
		string s2 = "-";
		for (int i = 0; i < rowCount * colCount; i++) {
			s += "- " + i / rowCount + "X" + i % rowCount + "(G):";
			s2 += "- " + i / rowCount + "X" + i % rowCount + "(C):";
			foreach (int id in emptyItemTypes [i / rowCount, i % rowCount]) {
				s += id + ",";
			}
			foreach (int id in boardCells[i/rowCount,i%rowCount].emptyItemTypes) {
				s2 += id + ",";
			}
			s += " -";
			s2 += " -";
		}

		Debug.Log (s);
		Debug.Log (s2);
	
	}


	void CallToAnimations (int row, int col)
	{

//		print (row +"," +  col);
		// 0 RowLine, 1 ColLine, 2 leftTopToRightBottomLine, 3 RightTopToLeftBottomLine, 4 stack
		if (animationStack.Count <= 0) {
			GameManager.ScoreManager.ScoreMult = 0;

		} 

//		else if (animationStack.Count > 1) {
//			comboTextManager.AnimateComboText (Random.Range (0, animationStack.Count));
//		}

		while (animationStack.Count > 0) {
			


			GameManager.ScoreManager.ScoreMult++;
			AnimationType animationElement = animationStack.Pop ();
			Vector3 position = boardCells [row, col].transform.position;
			//print (position);
			switch (animationElement.type) {

			// Create Animation at Col
			case 0:
				position = boardCells [1, col].transform.position;
				ParticleSystemManager.PlayTrailEffect (ParticleSystemManager.trailEffect, position, FXQ_MoveParticle.eMoveMethod.UpDown, animationElement.colorID);
				break;
			// Create Animation at Row
			case 1:
				position = boardCells [row, 1].transform.position;
				ParticleSystemManager.PlayTrailEffect (ParticleSystemManager.trailEffect, position, FXQ_MoveParticle.eMoveMethod.LeftRight, animationElement.colorID);
				break;
			// Create Animation leftTop corner To RightBottom corner
			case 2:
				position = boardCells [1, 1].transform.position;
				ParticleSystemManager.PlayTrailEffect (ParticleSystemManager.trailEffect, position, FXQ_MoveParticle.eMoveMethod.CrossUpDownNegative, animationElement.colorID);
				break;
			// Create Animation RightTop corner To leftBottom corner
			case 3:
				position = boardCells [1, 1].transform.position;
				ParticleSystemManager.PlayTrailEffect (ParticleSystemManager.trailEffect, position, FXQ_MoveParticle.eMoveMethod.CrossUpDown, animationElement.colorID);
				break;
			case 4:
				ParticleSystemManager.PlayEffect (ParticleSystemManager.stackEffect, position, animationElement.colorID);
//				SoundManager.PlaySfx2 (SoundManager.bubbleAudio);
				break;

			default:
				break;
			}

		}

		if (GameManager.ScoreManager.ScoreMult > 1) {
			scoreMultiplierContainer.SetActive (true);

			scoreMultText.text = "x" + Mathf.CeilToInt (GameManager.ScoreManager.ScoreMult).ToString ();

			comboTextManager.ChangeScaleTween (scoreMultText, 1.4f, 0.5f, () => EndOfTheScale (fontSizeScoreMult), 1);
			comboTextManager.AnimateComboText (GameManager.ScoreManager.ScoreMult - 2);
			//scoreMultText.gameObject.SetActive (true);
		} else {
			scoreMultiplierContainer.SetActive (false);
			//scoreMultText.gameObject.SetActive (false);
		}
	}

	void EndOfTheScale (int fontSize)
	{
		scoreMultText.fontSize = fontSize;
	}

}
