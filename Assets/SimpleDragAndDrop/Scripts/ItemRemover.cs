﻿using UnityEngine;
using System.Collections;

public class ItemRemover
{

	public int row;
	public int col;

	public int colorID;

	public ItemRemover (int row, int col, int colorID)
	{
		this.row = row;
		this.col = col;
		this.colorID = colorID;
	}
}
