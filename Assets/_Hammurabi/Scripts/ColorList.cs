﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColorList : MonoBehaviour
{
	private bool isNormalListUsed;

	void Awake ()
	{
//		isNormalListUsed = SaveManager.GetColorListOption ();
	}

	public bool IsNormalListUsed {
		get {
			return isNormalListUsed;
		}
		set {
			isNormalListUsed = value;
//			print (value);
		}
	}

	//	private List<Color32> colorList = new List<Color32> ();

	[Header ("Default Color Palette")]
	private Color32[] colorNormalList = {
		// Deep Red
		new Color32 (248, 72, 17, 255),
		//new Color32 (255, 23, 68, 255),
		// Orange
		new Color32 (255, 152, 0, 255),
		// Light Green
		new Color32 (4, 213, 0, 255),
		// Yellow
		new Color32 (255, 235, 59, 255),
		// Light Blue
		new Color32 (66, 250, 250, 255),
		// Blue Grey
//		new Color32 (96, 125, 139, 255),
		// Teal
		//new Color32 (0, 150, 136, 255),
		// Pink
		new Color32 (252, 86, 196, 255),
		// Indigo
		new Color32 (32, 142, 231, 255),
		// Purple
		new Color32 (166, 76, 248, 255)
	};
		
	[Header ("ColorBlind Color Palette")]
	private Color32[] colorBlindList = {
		// CB White
		new Color32 (255, 255, 255, 255),
		// CB Orange
		new Color32 (224, 156, 36, 255),
		// CB Sky Blue
		new Color32 (93, 179, 229, 255),
		// CB Green
		new Color32 (36, 154, 106, 255),
		// CB Yellow
		new Color32 (238, 225, 81, 255),
		// CB Blue
		new Color32 (60, 87, 150, 255),
		// CB Red
		new Color32 (138, 27, 30, 255),
		// CB Pink
		new Color32 (211, 143, 159, 255)
	};


	public Color32[] GetColorList ()
	{
		if (isNormalListUsed)
			return colorNormalList;
		else
			return colorBlindList;
		
	}
}
