﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SimpleTween;
using System;
using System.Collections.Generic;

public class ComboTextManager : MonoBehaviour
{
	private Text comboText;
	private Stack<Tween>[] tweenStackArray;
	int fontSize;

	private string[] comboTextArray = {
		"Good Job", // "GOOD JOB!",  GUZEL
		"Great", //"GREAT!",  BRAVO
		"Perfect", //"PERFECT!",  ETKILEYICI
		"Awesome", //"AWESOME!!",  MUKEMMEL
		"Outstanding", //"OUTSTANDING!!!",  O DA NESI
		"Crazy", //"CRAZY!!!!",  CILGIN
		"Legendary" //"LEGENDARY!!!!" / EFSANE
	};

	public void ChangeScaleTween (Text comboText, float multiply, float time, Action myMethodName, int tweenType)
	{
		if (tweenType == 0 || tweenType == 1) {
			
			while (tweenStackArray [tweenType].Count > 0) {
				SimpleTweener.RemoveTween (tweenStackArray [tweenType].Pop ());
			}
			myMethodName ();
			if (tweenType == 0) {
				comboText.enabled = true;
			} 
			int fontSize = comboText.fontSize;
			Tween tweenToUse = null;
			tweenToUse = SimpleTweener.AddTween (() => comboText.fontSize, x => comboText.fontSize = x, Convert.ToInt32 (comboText.fontSize * multiply), time).Ease (Easing.EaseOut).OnCompleted (() => {
				tweenToUse = SimpleTweener.AddTween (() => comboText.fontSize, x => comboText.fontSize = x, comboText.fontSize, time).OnCompleted (() => {
					tweenToUse = SimpleTweener.AddTween (() => comboText.fontSize, x => comboText.fontSize = x, Convert.ToInt32 (fontSize / multiply), time / 2).Ease (Easing.EaseIn).OnCompleted (() => {
						myMethodName ();
					});
					tweenStackArray [tweenType].Push (tweenToUse);
				});
				tweenStackArray [tweenType].Push (tweenToUse);
			});
			tweenStackArray [tweenType].Push (tweenToUse);
		}
	}

	void EndOfTheScale (int fontSize)
	{
//							comboText.text = "";
		comboText.enabled = false;
		comboText.fontSize = fontSize;
	}

	// Use this for initialization
	void Start ()
	{
		tweenStackArray = new Stack<Tween>[2];
		tweenStackArray [0] = new Stack<Tween> ();
		tweenStackArray [1] = new Stack<Tween> ();
		comboText = transform.Find ("ComboText").GetComponent<Text> ();
		fontSize = comboText.fontSize;
	}

	public void AnimateComboText (int animationNumber)
	{
		if (animationNumber >= comboTextArray.Length || animationNumber < 0)
			animationNumber = comboTextArray.Length - 1;

		comboText.text = I2.Loc.ScriptLocalization.Get (comboTextArray [animationNumber]);
//		print ("GIRDI MI");
		comboText.enabled = true;
//		comboText.fontSize = 75;

		ChangeScaleTween (comboText, 1.3f, 0.7f, () => EndOfTheScale (fontSize), 0);
//		StartCoroutine (WaitForText ());

	}

	//	IEnumerator WaitForText ()
	//	{
	//		ChangeScaleTween (1.6f, 0.5f);
	//		yield return new WaitForSeconds (1.0f);
	//		ChangeScaleTween (0.01f, 0.5f);
	//		comboText.text = "";
	//		comboText.enabled = false;
	//
	//
	//	}
}
