﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;



/// <summary>
/// Main Game Manager, handles the overall state of the game. 
/// Initializes in the HammurabiLogo scene and wont be destroyed.
/// </summary>
public class GameManager : MonoBehaviour
{

	// Event For The Close Calls
	//	public Action <bool> OnCloseCall{ get; set; }

	// Event For the Power Up Collect
	//	public Action <int> OnPowerUpCollected{ get; set; }

	// Event For the Power Up use
	//	public Action <int,float,float> OnPowerUpClicked{ get; set; }

	// Current active vehile index in the list
	//	private static int vehicleCurIndex;

	private static bool isDragStarted = false;

	public static bool IsDragStarted {
		get {
			return isDragStarted;
		}
		set {
			isDragStarted = value;
		}
	}

	private static ColorList colorList;

	public GameObject iconObject;

	public delegate void GameOver (bool condition);

	static public event GameOver OnGameOver;
	public static bool isFirstGame = true;

	// Game States
	public enum GameState
	{
		InMenus,
		Playing,
		Paused,
		Dead
	}

	//	// Game Modes
	//	public enum GameMode
	//	{
	//		Endless = 0,
	//		EndlessTwoWay = 1,
	//		TimeAttack = 2,
	//		FreeMode = 3
	//	}

	// Current Game State
	public static GameState state;

	// Current Game Mode
	//	public static GameMode gameMode;

	// Current Scene
	//	private Scene currentScene;

	// references to our player and level manager.
	//	private PlayerControl player;
	//	private LevelManager levelManager;
	private static ScoreManager scoreManager;
	//	private MenuManager menuManager;


	// Bu burada olmamali!!! Haritanin hizli akma sorununu cozuyor.
	//	public float speedDecreaserRatio = 0.5f;

	// Selected Car Index
	//	private static int selectedCarIndex;

	// Name of the Level
	//	private static string levelName;

	//	public static bool crashOccured = false;

	// Bu burada olmamali!!! Range for Creation of the Cars
	//	public int rangeForCreation = 150;
	//	public int rangeForCreationBack = -50;

	/// <summary>
	/// Get a reference to the player object
	/// </summary>
	/// <value>The player object</value>
	//	public static PlayerControl Player {
	//		get { return instance.player; }
	//	}
	//
	//	/// <summary>
	//	/// Get a reference to the level manager
	//	/// </summary>
	//	/// <value>The level manager</value>
	//	public static LevelManager LevelManager {
	//		get { return instance.levelManager; }
	//	}

	/// <summary>
	/// Get a reference to the score manager
	/// </summary>
	/// <value>The level manager</value>


	//	// Get the Current Scene
	//	public Scene CurrentScene {
	//		get {
	//			return currentScene;
	//		}
	//		set {
	//			currentScene = value;
	//		}
	//	}

	public static ColorList ColorList {
		get {
			return colorList;
		}
		set {
			colorList = value;
		}
	}

	public static ScoreManager ScoreManager {
		get {
			return scoreManager;
		}
		set {
			scoreManager = value;
		}
	}

	//	public static MenuManager MenuManager {
	//		get { return instance.menuManager; }
	//	}



	/// <summary>
	/// Get the current game state
	/// </summary>
	/// <value>The current game state</value>
	public static GameState CurrentState {
		get { return state; }
	}

	/// <summary>
	/// Gets or sets the record distance.
	/// </summary>
	/// <value>The record distance.</value>
	//	public static float RecordDistance {
	//		get { return PlayerPrefs.GetFloat ("RecordDistance", 0.0f); }
	//		set { PlayerPrefs.SetFloat ("RecordDistance", value); }
	//	}



	public static GameManager instance;

	void Awake ()
	{
		//PlayerPrefs.DeleteAll ();
		//For performance and heating related problems.
		Application.targetFrameRate = 30;

		if (!instance) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else
			Destroy (gameObject);

//		currentScene = SceneManager.GetActiveScene ();
		colorList = GetComponent<ColorList> ();
//		menuManager = GameObject.Find ("MainCanvas").GetComponent<MenuManager> ();
//		scoreManager = menuManager.GetComponent<ScoreManager> ();
//		SceneManager.sceneLoaded += OnLevelLoadFinished;

	}

	void Start(){
		SaveManager.IncreaseStartCount ();
	}

	//	void OnDestroy()
	//	{
	//		//Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
	//		SceneManager.sceneLoaded -= OnLevelLoadFinished;
	//	}

	//	void OnLevelLoadFinished (Scene scene, LoadSceneMode mode)
	//	{
	////		currentScene = SceneManager.GetActiveScene ();
	////		print ("giriyor mu");
	////		menuManager = GameObject.Find ("MainCanvas").GetComponent<MenuManager> ();
	////		scoreManager = menuManager.GetComponent<ScoreManager> ();
	//	
	//	}


	// Bu burada olmamali!!!
	public void ActivateQuitPopUp ()
	{
		PopUpMenu.instance.ActivateQuitPopUp ();
	}

	// Start the Main Menu
	//	void StartMainMenu ()
	//	{
	//
	//		menuManager = GameObject.FindObjectOfType<MenuManager> ();
	//		menuManager.OnEnterStateMenus ();
	//
	////		levelManager = GameObject.FindObjectOfType<LevelManager> ();
	//
	//		state = GameState.InMenus;
	//	}

	void StartLevel ()
	{
//		Screen.sleepTimeout = SleepTimeout.NeverSleep;
//		// Start The Menu Arrangements for game load
//		menuManager = GameObject.FindObjectOfType<MenuManager> ();
//		menuManager.OnStartGamePressed ();
//
//
//		// start the level scrolling!
//		//levelManager.StartGame ();
//
//		// Set State to Playing
//		state = GameState.Playing;

	}


	//	public static string LevelName {
	//		get {
	//			return levelName;
	//		}
	//		set {
	//			levelName = value;
	//		}
	//	}


	public void GameOverMethod ()
	{

//		print ("End of the game");
		StartCoroutine (GameOverWithTime (0.5f));
//		menuManager.OnEnterStateDeath ();
		state = GameState.Dead;

			
	}

	IEnumerator GameOverWithTime (float time)
	{
		yield return new WaitForSeconds (time);
		ScreenShotTaker.TakeScreenShot ();
		if (OnGameOver != null)
			OnGameOver (true);
//		MenuManager.OnEnterStateDeath ();
	}


}
