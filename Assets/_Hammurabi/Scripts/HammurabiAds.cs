﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using UnityEngine.SceneManagement;

public class HammurabiAds: MonoBehaviour
{
	// Ids Of the Ad service
	static string interstitialID;
	static string bannerID;
	static string rewardedVideoID;


	static RewardBasedVideoAd rewardBasedVideo;

	// View objects of the Ad service
	static InterstitialAd interstitialView;
	static BannerView bannerView;

	// Requestor object for the Ads
	static AdRequest adRequestor;

	// Control variable for Interstitial Load
	//	static bool isInterstitialLoaded = false;

	static HammurabiAds instance;

	public static HammurabiAds Instance { get { return instance; } }

	// Initialization
	void Awake ()
	{

		Initialization ();


	}

	void Initialization ()
	{

		instance = this;
		SceneManager.sceneLoaded += OnLevelLoadFinished;

		#if UNITY_EDITOR
		interstitialID = "unused";
		#elif UNITY_ANDROID
		//TEST ID
//		interstitialID = 	"ca-app-pub-3940256099942544/1033173712";
//		bannerID = 			"ca-app-pub-3940256099942544/6300978111";
		//REAL ID
		interstitialID = 	"ca-app-pub-7160710119740205/2613069570";
		bannerID = 			"ca-app-pub-7160710119740205/4369004375";
		#elif UNITY_IPHONE
		// TEST ID
//		interstitialID = 	"ca-app-pub-3940256099942544/4411468910";
//		bannerID = 			"ca-app-pub-3940256099942544/6300978111";
		// REAL ID
		interstitialID = 	"ca-app-pub-7160710119740205/6764067577";
		bannerID = 			"ca-app-pub-7160710119740205/2333867974";
		#else
		interstitialID  = 	"unexpected_platform";
		#endif

		OnCreate ();
		RequestBanner ();

	}

	//	private void LoadRewardBasedVideo ()
	//	{
	//
	//
	//		rewardBasedVideo = RewardBasedVideoAd.Instance;
	//		rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
	//
	//		AdRequest request = new AdRequest.Builder ().Build ();
	//		rewardBasedVideo.LoadAd (request, rewardedVideoID);
	//
	//		//		// Reward based video instance is a singleton. Register handlers once to
	//		//		// avoid duplicate events.
	//		//		if (!rewardBasedEventHandlersSet) {
	//		//		// Ad event fired when the rewarded video ad
	//		//		// has been received.
	//		//		rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
	//		//		// has failed to load.
	//		//		rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
	//		//		// is opened.
	//		//		rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
	//		//		// has started playing.
	//		//		rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
	//		//		// has rewarded the user.
	//		//		rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
	//		//		// is closed.
	//		//		rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
	//		//		// is leaving the application.
	//		//		rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
	//		//
	//		//		rewardBasedEventHandlersSet = true;
	//		//		}
	//
	//
	//	}

	//	public void HandleRewardBasedVideoRewarded (object sender, Reward args)
	//	{
	//		string type = args.Type;
	//		double amount = args.Amount;
	//		SaveManager.AddCoins (2500);
	////		print ("User rewarded with: " + amount.ToString () + " " + type);
	//	}

	//	public void ShowRewardedVideo ()
	//	{
	//		if (rewardBasedVideo.IsLoaded ()) {
	//			rewardBasedVideo.Show ();
	//		}
	//	}

	void GetHashedId ()
	{
		//		String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
		//		String deviceId = md5(android_id).toUpperCase();
		//		Log.i("device id=",deviceId);
	}


		void OnDestroy()
		{
		//Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
		SceneManager.sceneLoaded -= OnLevelLoadFinished;
		}

	// Whenever Level changes load Interstitial Ad
		void OnLevelLoadFinished (Scene scene, LoadSceneMode mode)
	{
		Initialization ();
		//		LoadInsterstitialAd ();
	}

	private void RequestBanner ()
	{
		// Create a 320x50 banner at the top of the screen.
		BannerView bannerView = new BannerView (bannerID, AdSize.SmartBanner, AdPosition.Top);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder ().Build ();
		// Load the banner with the request.
		bannerView.LoadAd (request);
	}

	public static void OnCreate ()
	{
		interstitialView = new InterstitialAd (interstitialID);
		interstitialView.OnAdClosed += HandleAdClosed;

		LoadInsterstitialAd ();
	}

	// Load Interstitial Ad Algorithm
	public static void LoadInsterstitialAd ()
	{
		// If Interstitial is not loaded b4, create new one and load it.
		if (!interstitialView.IsLoaded ()) {
//			interstitialView =  new InterstitialAd (interstitialID);
//			interstitialView.OnAdLoaded += HandleAdLoaded;
//			interstitialView.OnAdClosed += HandleAdClosed;
			AdRequest adRequest = new AdRequest.Builder ()
		.AddTestDevice (AdRequest.TestDeviceSimulator)        // All emulators
			                      //					.AddTestDevice("AC98C820A50B4AD8A2106EDE96FB87D4")  // An example device ID
		.Build ();
			interstitialView.LoadAd (adRequest);
		}
	}
	//
	//	// Whenever Ad loaded, set isInterstitialLoaded to TRUE
	//	public static void HandleAdLoaded (object sender, System.EventArgs args)
	//	{
	//		isInterstitialLoaded = true;
	//	}

	// Whenever Ad closed, Destroy object and set isInterstitialLoaded to false and reload a new interstitial.
	public static void HandleAdClosed (object sender, System.EventArgs args)
	{
//		interstitialView.Destroy ();
//		isInterstitialLoaded = false;
		LoadInsterstitialAd ();
	}

	// Demonstration of the Interstitial Ad with the probability of "percantage" input
	public static void ShowInterstitialAd (int percantage)
	{
		if (!SaveManager.GetRemoveAds () && SaveManager.GetCompletedGameCount () > 2) {
			try {
				if (interstitialView.IsLoaded ()) {
					int randomNumber = Random.Range (1, 100);
					if (randomNumber <= percantage) {
						interstitialView.Show (); 
//		HammurabiGoogleAnalytics.Instance.LogEvent("Ad Units", "Interstitial",percantage +"% Percentage" ,1);
					}
				} 
			} catch (System.Exception e) {
				Debug.Log (e.ToString ());
			}

		}
	}

	// Show Banner Ad
	public static void ShowBannerAd ()
	{
		try {
			if (adRequestor != null)
				bannerView.LoadAd (adRequestor);
			bannerView.Show ();
		} catch (System.Exception e) {
			Debug.Log (e.ToString ());
		}
		;
	}
	
	// Hide Banner Ad
	public static void HideBannerAd ()
	{
		bannerView.Hide ();
	}
	
	// Destroy Banner Ad
	public static void DestroyBannerAd ()
	{
		bannerView.Destroy ();
	}
}

