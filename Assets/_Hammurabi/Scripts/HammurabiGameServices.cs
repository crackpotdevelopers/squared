﻿using UnityEngine;
using System.Collections;

//using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;

public class HammurabiGameServices : MonoBehaviour
{

	// AudioManager Created Instance Getter Setter
	public static HammurabiGameServices Instance
	{ get { return instance; } }

	private static HammurabiGameServices instance = null;

	void Awake ()
	{
		instance = this;
		PlayGamesPlatform.Activate ();
	}

	// Use this for initialization
	void Start ()
	{
		// recommended for debugging:
		//PlayGamesPlatform.DebugLogEnabled = true;

		// Activate the Google Play Games platform


		Social.localUser.Authenticate ((bool success) => {
			if (success) {
				Debug.Log ("Google Play Services: Authentication Succeed");
				try {
			
					PushScoreToLeaderBoard ("Level01", (int)SaveManager.GetLevelScore ("Level01"));

				} catch (System.Exception e) {

					Debug.Log (e.ToString ());
				}
			} else
				Debug.Log ("Google Play Services: Authentication Failed");
		});
	}

	public void ShowLeaderBoard ()
	{
		Social.ShowLeaderboardUI ();
		//print("hello");
	}

	public void ShowAchievements ()
	{
		Social.ShowAchievementsUI ();
	}

	public void UnlockAchievement (int index)
	{
		try {
			string achievementID = "";

//			switch (index){
//			case 1: achievementID = GooglePlayConstants.achievement_Drift_x3	;break;
//			case 2: achievementID = GooglePlayConstants.achievement_Drift_x5	;break;
//			case 3: achievementID = GooglePlayConstants.achievement_Drift_x10	;break;
//			case 4: achievementID = GooglePlayConstants.achievement_First_Car	;break;
//			case 5: achievementID = GooglePlayConstants.achievement_Having_5_Car_Keys	;break;
//			case 6: //achievementID = GooglePlayConstants.achievement_Perfect_Drifter	;
//				break;
//			case 7: achievementID = GooglePlayConstants.achievement_Fully_Modified_Car	;break;
//			case 8: achievementID = GooglePlayConstants.achievement_Completing_2_Levels	;break;
//			case 9: achievementID = GooglePlayConstants.achievement_Completing_5_Levels	;break;
//			case 10: achievementID = GooglePlayConstants.achievement_Beginner	;break;
//			case 11: achievementID = GooglePlayConstants.achievement_Driver	;break;
//			case 12: achievementID = GooglePlayConstants.achievement_Drifter	;break;
//			}
			// unlock achievement (achievement ID "Cfjewijawiu_QA")
			Social.ReportProgress (achievementID, 100.0f, (bool success) => {
				//print ("BURDA " + success);
				// handle success or failure
			});

			//			Debug.Log("Achievement " + index + " Unlocked");
		} catch (System.Exception e) {
			Debug.Log (e.ToString ());
		}

	}

	public void PushScoreToLeaderBoard (string levelName, float score)
	{
		try {
			string leaderboardID = "";
			switch (levelName) {
			case "Level01":
				leaderboardID = GooglePlayConstants.leaderboard_bare_block;
				break;

			}
//			}

			if (score > 0 && !string.IsNullOrEmpty (leaderboardID)) {
				// post score 12345 to score leaderboard ID "Cfji293fjsie_QA")
				Social.ReportScore ((long)score, leaderboardID, (bool success) => {
					// handle success or failure
				});
			}

		} catch (System.Exception e) {
			Debug.Log (e.ToString ());
		}
	}


}
