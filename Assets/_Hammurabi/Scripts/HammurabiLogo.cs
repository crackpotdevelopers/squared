﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SimpleTween;
using UnityEngine.SceneManagement;

//using I2.Loc;
using CodeStage.AntiCheat.ObscuredTypes;

public class HammurabiLogo : MonoBehaviour
{

	private GameObject loadingText;
	private Image faderImage;
	private Color fadeColor;

	// Use this for initialization
	void Start ()
	{
		loadingText = transform.Find ("LoadingText").gameObject;
		faderImage = transform.Find ("Fader").GetComponent<Image> ();
		fadeColor = faderImage.color;

		StartCoroutine (StartCrossFadeIn ());

//		if (!ObscuredPrefs.HasKey ("LanguageOption")) {
//			if (Application.systemLanguage == SystemLanguage.Turkish && LocalizationManager.HasLanguage ("Turkish")) {
//				LocalizationManager.CurrentLanguage = "Turkish";
//				SaveManager.SetLanguageSetting ("Turkish");
//			} else if (Application.systemLanguage == SystemLanguage.German && LocalizationManager.HasLanguage ("German")) {
//				LocalizationManager.CurrentLanguage = "German";
//				SaveManager.SetLanguageSetting ("German");
//			} else if (Application.systemLanguage == SystemLanguage.Portuguese && LocalizationManager.HasLanguage ("Portuguese")) {
//				LocalizationManager.CurrentLanguage = "Portuguese";
//				SaveManager.SetLanguageSetting ("Portuguese");
//			} else {
//				LocalizationManager.CurrentLanguage = "English";
//				SaveManager.SetLanguageSetting ("English");
//			}
//		} else {
////			print("Eng");
//			LocalizationManager.CurrentLanguage = SaveManager.GetLanguageSetting ();
//		}



	}

	IEnumerator StartCrossFadeIn ()
	{
		yield return null;
		fadeColor.a = 0.0f;

		SimpleTweener.AddTween (() => faderImage.color, x => faderImage.color = x, fadeColor, 1.0f).Ease (Easing.EaseLinear).OnCompleted (() => {
			StartCoroutine (StartCrossFadeOut ());
		});
	
	}

	IEnumerator StartCrossFadeOut ()
	{
		#if UNITY_ANDROID
		yield return new WaitForSeconds (2.0f);
		#elif UNITY_IPHONE
		yield return new WaitForSeconds(4.0f);
		#else
		yield return new WaitForSeconds (2.0f);
		#endif
//		loadingText.text = LocalizationManager.instance.GetWord("Loading");
		loadingText.SetActive (true);
		fadeColor.a = 1.0f;

		SimpleTweener.AddTween (() => faderImage.color, x => faderImage.color = x, fadeColor, 1.0f).Ease (Easing.EaseLinear).OnCompleted (() => {
			SceneManager.LoadScene ("Level");
		});
			
	}

	//	IEnumerator StartTheGame(){
	//		yield return null;
	//
	//		SceneManager.LoadScene("MainMenu_Racer");
	//
	//	}

}
