﻿using UnityEngine;
using System.Collections;
using System;

public class HammurabiToggle : MonoBehaviour {

	private GameObject[] toggleOnOffImages;
	public bool isImageOnMeansOn = false;
	private string toggleName;
	public delegate void ToggleChangeEvent (int index);

	public event ToggleChangeEvent OnToggleOnOff;

	void Awake ()
	{
		toggleOnOffImages = new GameObject[transform.childCount];
		for (int i = 0; i < transform.childCount; i++) {
			toggleOnOffImages[i] = transform.GetChild(i).Find ("ToggleOnOff").gameObject;
		}
		toggleName = gameObject.name;

	}

	public void Init(){
		if (toggleOnOffImages.Length == 1) {
			toggleOnOffImages[0].SetActive (!Convert.ToBoolean(SaveManager.GetToggleSetting(toggleName))^isImageOnMeansOn);
			ChangeToggleSetting (0);
		} else {
			ChangeToggleSetting (SaveManager.GetToggleSetting (toggleName));
		}
	}

	public void ChangeToggleSetting (int index)
	{
		if (toggleOnOffImages.Length == 1) {
			toggleOnOffImages[0].SetActive (!toggleOnOffImages[0].activeSelf^isImageOnMeansOn);
			SaveManager.SetToggleSetting (toggleName, Convert.ToInt32(toggleOnOffImages[0].activeSelf));
			if (OnToggleOnOff != null) {
				OnToggleOnOff (Convert.ToInt32(toggleOnOffImages[0].activeSelf));
			}
		} else {
			for (int i = 0; i < transform.childCount; i++) {
				if (i == index) {
					toggleOnOffImages [i].SetActive (!isImageOnMeansOn);
					SaveManager.SetToggleSetting (toggleName, index);
					if (OnToggleOnOff != null) {
						OnToggleOnOff (index);
					}
				} else {
					toggleOnOffImages [i].SetActive (isImageOnMeansOn);
				}
			}
		}
	}
		
}
