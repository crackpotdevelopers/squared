﻿using UnityEngine;
using System.Collections;
using SimpleTween;
using UnityEngine.SceneManagement;
using Fabric.Answers;
using System.Collections.Generic;

public class MenuManager : MonoBehaviour
{


	// references to various menu screens
	private MenuSystem menuSystem;
	private static MenuManager instance;
	private GameObject topMenuBar;
	private GameObject loadingScreen;
	private CanvasGroup screenFade;
	private RestartMenu restartMenu;
	private Transform levelMenu;
	private GameLogic gameLogic;
	private bool isGameOver;

	// Use this for initialization
	void Awake ()
	{
		instance = this;
		menuSystem = GetComponent<MenuSystem> ();
		screenFade = transform.Find ("ScreenFade").GetComponent<CanvasGroup> ();
		restartMenu = transform.Find ("RestartMenu").GetComponent<RestartMenu> ();
		//topMenuBar = transform.Find ("TopMenuBar").gameObject;
		loadingScreen = transform.Find ("LoadingScreen").gameObject;
		levelMenu = transform.Find ("LevelMenu");
		gameLogic = levelMenu.GetComponent<GameLogic> ();
		isGameOver = false;

	}

	void OnEnable ()
	{
		GameManager.OnGameOver += OnEnterStateDeath;
	}

	void OnDisable ()
	{
		GameManager.OnGameOver -= OnEnterStateDeath;
	}

	void Start ()
	{
		// start with the screen fader opaque
		//screenFade.alpha = 1.0f;

		ShowLoadingScreen (false);

		OnShowMainMenu ();
	}

	void Update ()
	{
	
		if (Input.GetKeyDown (KeyCode.Escape)) { 
//			print ("State: " + GameManager.CurrentState);
			if (GameManager.CurrentState == GameManager.GameState.Playing) {
				this.PauseGame ();
			} else if (GameManager.CurrentState == GameManager.GameState.Paused) {
				if (PopUpMenu.instance.IsPopUpActive)
					PopUpMenu.instance.Hide ();
				else
					GoBack (1);
			} else if (GameManager.CurrentState == GameManager.GameState.InMenus) {
				if (PopUpMenu.instance.IsPopUpActive)
					PopUpMenu.instance.Hide ();
				else
					GoBack (0);
			} else if (GameManager.CurrentState == GameManager.GameState.Dead) {
				OnShowMainMenu ();
//				if (menuSystem.GetScreenCount () > 2)
//					GoBack ();
//				else
//					this.LoadMainMenu ();
			} else {
				Debug.Log ("Error: Unknown Game State");
			}
		}
	}



	public static MenuManager Instance {
		get {
			return instance;
		}
		set {
			instance = value;
		}
	}

	private void ScreenFaderFadeIn (float time, SimpleTween.Callback onCompletedCB)
	{

		// fade the screen to white
		screenFade.gameObject.SetActive (true);
		screenFade.alpha = 0.0f;
		SimpleTweener.AddTween (() => screenFade.alpha, x => screenFade.alpha = x, 1.0f, time).OnCompleted (onCompletedCB);
	}

	private void ScreenFaderFadeOut (float time)
	{
		// fade out the white overlay
		if (screenFade.gameObject.activeSelf)
			SimpleTweener.AddTween (() => screenFade.alpha, x => screenFade.alpha = x, 0.0f, time).OnCompleted (() => screenFade.gameObject.SetActive (false));

	}

	//	public void OnEnterStateMenus ()
	//	{
	//
	//		//topMenuBar.SetActive (true);
	//		// show the main menu screen.
	//		menuSystem.ShowScreen (menuSystem.MainMenu, 0.5f);
	//
	//
	//		SoundManager.PlayMusic (SoundManager.introLoopAudio);
	//		ScreenFaderFadeOut (1.0f);
	//	}

	// Menu arrangements for the game start
	public void OnEnterStateGame ()
	{
		// Close The Top Menu
		//topMenuBar.SetActive (false);




		// Fade Out to Start the Game
		ScreenFaderFadeOut (0.4f);
	}

	public void OnEnterStateDeath (bool condition)
	{
		isGameOver = true;

		//topMenuBar.SetActive (true);
		restartMenu.SetHighestScore (GameManager.ScoreManager.TotalScore);
//		restartMenu.SetScore(GameManager.ScoreManager)
		menuSystem.ShowScreen (menuSystem.RestartMenu, 0.5f);
		Answers.LogLevelEnd (SceneManager.GetActiveScene ().name, GameManager.ScoreManager.TotalScore, true, null); 
		SaveManager.IncreaseCompletedGameCount ();

//
//
//		crossPlatformCanvas.alpha = 0;
//		crossPlatformCanvas.blocksRaycasts = false;
//		GameManager.LevelManager.FlareManager.StopFlares ();
	}

	//	public void OnWatchVideoCompleted ()
	//	{
	//		restartMenu.OnWatchVideoCompleted ();
	//	}

	//	private IEnumerator ShowAd ()
	//	{
	////		yield return new WaitForSeconds (1.0f);
	//		HammurabiAds.ShowInterstitialAd (100);
	//	}
	//
	public void OnShowMainMenu ()
	{
		GameManager.isFirstGame = true;
		GameManager.state = GameManager.GameState.InMenus;
		//SoundManager.PlaySfx (SoundManager.buttonPressedAudio);
		menuSystem.ExitAll ();
		ScreenFaderFadeOut (0.5f);
		menuSystem.ShowScreen (menuSystem.MainMenu, 0.5f);
		//ScreenFaderFadeIn (0.8f, () => OnEnterStateMenus ());
		PopUpMenu.instance.MenuType = 0;
	}


	public void DeletePlayerPrefs ()
	{
		SaveManager.DeletePlayerPrefs ();
	}
	// When the Level Loaded, Enter the Playing Game State (Method Called After Level Load not button pressed)
	public void OnStartGamePressed ()
	{
		isGameOver = false;
		GameManager.state = GameManager.GameState.Playing;
		SoundManager.PlaySfx (SoundManager.buttonPressedAudio);
		menuSystem.ExitAll ();
		menuSystem.ShowScreen (menuSystem.LevelScreenMenu, 0.5f);
		ScreenFaderFadeOut (1f);
		//ScreenFaderFadeIn (0.8f, () => OnEnterStateGame ());
		ShowLoadingScreen (false);

		Answers.LogLevelStart (
			SceneManager.GetActiveScene ().name,
			null
		);

		if (GameManager.isFirstGame) {
			GameManager.isFirstGame = false;
			gameLogic.RestartTheGame ();
		}
	}

	public void OnSettingsPressed ()
	{
		if (menuSystem.SettingsMenu.gameObject.activeSelf == false) {
			SoundManager.PlaySfx (SoundManager.buttonPressedAudio);
//			menuSystem.ExitAll ();
			menuSystem.ShowScreen (menuSystem.SettingsMenu, 0.5f);
			//menuSystem.SettingsMenu.Show (0.3f);
		}
	}

	public void OnChangeColors ()
	{
		gameLogic.ChangeColors ();
	}

	public void OnHowToPlayButtonPressed ()
	{
		if (menuSystem.HowToPlayMenu.gameObject.activeSelf == false) {
			SoundManager.PlaySfx (SoundManager.buttonPressedAudio);
			//			menuSystem.ExitAll ();
			menuSystem.ShowScreen (menuSystem.HowToPlayMenu, 0.5f);
		}
	}

	//	public void OnShowLevelMenuPressed ()
	//	{
	//		SoundManager.PlaySfx (SoundManager.buttonPressedAudio);
	//		menuSystem.ShowScreen (menuSystem.LevelScreenMenu, 0.5f);
	//		PopUpMenu.instance.MenuType = 1;
	//	}



	public void PauseGame ()
	{
		//topMenuBar.SetActive (true);

		if (GameManager.state != GameManager.GameState.Playing)
			return;

//		Time.timeScale = 0.0f;
		GameManager.state = GameManager.GameState.Paused;
//		isGameOver = false;

		SoundManager.PlaySfx (SoundManager.buttonPressedAudio);
		menuSystem.ShowScreen (menuSystem.PauseMenu, 0.3f);
		//SoundManager.PauseBonusSfx ();

	}

	public void GoBack (int index)
	{
		SoundManager.PlaySfx (SoundManager.buttonPressedAudio);
		menuSystem.GoBack (index, 0.3f);
	}

	//	public void OnPopUpShow ()
	//	{
	//		menuSystem.ShowScreen (menuSystem.HudMenu, 0.3f);
	//	}


	public void ResumeGame ()
	{
		
		//topMenuBar.SetActive (false);
		ShowLoadingScreen (false);

//		Time.timeScale = 1.0f;
		GameManager.state = GameManager.GameState.Playing;
		menuSystem.GoBack (0, 0.3f);

		SoundManager.PlaySfx (SoundManager.buttonPressedAudio);
	}

	public void RestartCurrentLevel ()
	{
//		levelMenu.transform.Find ("BlockRaycaster").gameObject.SetActive (false);

		if (!isGameOver) {
			//print ("pushla false");
			Answers.LogLevelEnd (SceneManager.GetActiveScene ().name, GameManager.ScoreManager.TotalScore, false, null); 

		} else {
		
		}

		OnStartGamePressed ();
		gameLogic.RestartTheGame ();
	
		//Oyun bitmediyse ve restart attiysa pushla

//		Time.timeScale = 1;
//		ShowLoadingScreen (true);
//		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
//		Answers.LogLevelEnd (SceneManager.GetActiveScene ().name, GameManager.ScoreManager.TotalScore, false, null); 
		//OnStartGamePressed ();
	}


	public void LoadMainMenu ()
	{
		ShowLoadingScreen (true);
		SoundManager.PlaySfx (SoundManager.buttonPressedAudio);
		SceneManager.LoadSceneAsync ("MainMenu_Racer");

	
	}

	public MenuSystem MenuSystem {
		get {
			return menuSystem;
		}
		set {
			menuSystem = value;
		}
	}

	public void ShowLoadingScreen (bool condition)
	{

		if (condition == true)
			loadingScreen.SetActive (true);
		else
			loadingScreen.SetActive (false);
	}

	bool leftApp = false;

	void OnApplicationPause ()
	{
		leftApp = true;
	}

	public void RateTheApp ()
	{
//		ScreenshotTaker.TakeHiResShot ();
		//ScreensotTaker.TakeHiResShot ();
//		GetComponent<ScreenCapture> ().SaveScreenshot (CaptureMethod.RenderToTex_Synch, Application.dataPath + "/screen1.png");
//		Debug.Log ("RateTheApp");

		#if UNITY_ANDROID
		Application.OpenURL ("market://details?id=com.hammurabigames.bareblock");
		#elif UNITY_IPHONE
		Application.OpenURL ("itms-apps://itunes.apple.com/app/id1181336225");
		#endif

		Answers.LogCustom ("Rate The App Clicked", customAttributes: new Dictionary<string, object> () {
			{ "App Start Count", SaveManager.GetStartCount() },
			{ "Completed Game Count", SaveManager.GetCompletedGameCount() }
		});
		//SaveManager.AddCoins (2000);
		//transform.Find ("DailyBonusMenu/ScrollRectFree/ContentPanel/RateUs").gameObject.SetActive (false);
		//SaveManager.SetRateUs (true);
	}

	public void ShowLeaderBoard ()
	{
//		Debug.Log ("Leaderboard");
//		Texture2D imageToShare = GetComponent<ScreenCapture> ().GetScreenshot (CaptureMethod.RenderToTex_Synch);
//		if (imageToShare == null)
//			print ("image is null");
		//SPShareUtility.ShareMedia ("I have just scored", "Share Message", imageToShare);
		SoundManager.PlaySfx (SoundManager.buttonPressedAudio);

		HammurabiGameServices.Instance.ShowLeaderBoard ();
	}


	public void FacebookLike ()
	{
		StartCoroutine (OpenFacebookPage ());

		//		string facebookLink = "fb://page/1648749492034906"; //"https://www.facebook.com/sharer/sharer.php?u=" + Uri.EscapeUriString(appStoreLink);
		//		Application.OpenURL(facebookLink);
	}

	//https://twitter.com/intent/user?user_id=
	//twitter://user?user_id=

	public void TwitterFollow ()
	{
		StartCoroutine (OpenTwitterPage ());

		//		string twitterLink = "twitter://user?user_id=3801625576"; //"twitter:///user?screen_name=hammurabigames";
		//		Application.OpenURL(twitterLink);
	}

	//	public void WatchRewardedVideo ()
	//	{
	//		HammurabiUnityAds.Instance.ShowRewardedAd (-1);
	//	}

	IEnumerator OpenFacebookPage ()
	{
		#if UNITY_ANDROID
		Application.OpenURL ("fb://page/1648749492034906");
		#elif UNITY_IPHONE
		Application.OpenURL ("fb://profile/1648749492034906");
		#endif
		float startTime = Time.realtimeSinceStartup;
		while (Time.realtimeSinceStartup < startTime + 0.5f) {
			yield return null;
		}
		//		yield return new WaitForSeconds (0.5f);
		if (leftApp) {
			leftApp = false;
		} else {
			Application.OpenURL ("https://www.facebook.com/hammurabigames");
		}
		//SaveManager.AddCoins (2000);
		transform.Find ("DailyBonusMenu/ScrollRectFree/ContentPanel/LikeUsOnFacebook").gameObject.SetActive (false);
		SaveManager.SetFacebookLike (true);

	}

	IEnumerator OpenTwitterPage ()
	{
		Application.OpenURL ("twitter://user?user_id=3801625576");
		float startTime = Time.realtimeSinceStartup;
		while (Time.realtimeSinceStartup < startTime + 0.5f) {
			yield return null;
		}
		//		yield return new WaitForSeconds (0.5f);
		if (leftApp) {
			leftApp = false;
		} else {
			Application.OpenURL ("https://twitter.com/hammurabigames");
		}
		//SaveManager.AddCoins (2000);
		transform.Find ("DailyBonusMenu/ScrollRectFree/ContentPanel/TwitterFollow").gameObject.SetActive (false);
		SaveManager.SetTwitterFollow (true);
	}
	
}