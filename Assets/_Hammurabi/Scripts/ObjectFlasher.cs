﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObjectFlasher : MonoBehaviour
{

	// Use this for initialization
	void OnEnable ()
	{
		StartCoroutine (FlashObject ());
	}



	IEnumerator FlashObject ()
	{
		//yield return new WaitForSeconds (0.1f);
		while (true) {
			//print ("123");

			if (gameObject.GetComponent<Image> ().enabled == true) {
				gameObject.GetComponent<Image> ().enabled = false;
				gameObject.GetComponentInChildren<Text> ().enabled = false;
				//print ("girdi");
			} else {
				gameObject.GetComponent<Image> ().enabled = true;
				gameObject.GetComponentInChildren<Text> ().enabled = true;
				//print ("hayir");
			}

			yield return new WaitForSeconds (0.5f);
		}


	}
}
