﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticleSystemManager : MonoBehaviour
{
	public static string stackEffect = "2D_Buff_02";
	public static string trailEffect = "UI_Trail_01_Loop";

	public List<ParticleSystem> particleList;
	// Use this for initialization
	public static ParticleSystemManager instance;

	void Awake ()
	{
		if (!instance) {
			instance = this;
		}
	}

	void Start ()
	{
//		particleList = new List<GameObject> ();
//
//		foreach (Transform child in transform) {
//			particleList.Add (child.gameObject);
//
//		}
	}


	public static void PlayTrailEffect (string effectName, Vector3 newPosition, FXQ_MoveParticle.eMoveMethod method, int colorID)
	{
		foreach (ParticleSystem effect in instance.particleList) {
			if (effect.name == effectName) {

				GameObject particleEffect = (GameObject)Instantiate (effect.gameObject);
				//particleEffect.transform.SetParent ();
				particleEffect.transform.SetParent (instance.transform);
				particleEffect.GetComponent<FXQ_MoveParticle> ().IsActive = false;
				particleEffect.GetComponent<ParticleSystem> ().Stop ();
				//effect.GetComponent<ParticleSystem> ().SetColor ("TintColor", GameManager.ColorList.ColorNormalList [colorID]);
				//Renderer rend = ;
				particleEffect.GetComponent<ParticleSystemRenderer> ().material.SetColor ("_Color", GameManager.ColorList.GetColorList () [colorID]);
				particleEffect.transform.Find ("Trail").GetComponent<ParticleSystemRenderer> ().material.SetColor ("_Color", GameManager.ColorList.GetColorList () [colorID]);
				particleEffect.GetComponent<ParticleSystem> ().startColor = GameManager.ColorList.GetColorList () [colorID];

				particleEffect.GetComponent<FXQ_MoveParticle> ().ResetPositionToPoint (newPosition);
				//effect.localPosition = newPosition;
				particleEffect.GetComponent<FXQ_MoveParticle> ().m_MoveMethod = method;
				particleEffect.GetComponent<FXQ_MoveParticle> ().IsActive = true;
				particleEffect.GetComponent<ParticleSystem> ().Play ();
				SoundManager.PlaySfx2 (SoundManager.laserBeamPowerUpAudio);

				Destroy (particleEffect, 2f);
				return;
			}
		}

		Debug.LogWarning ("No effect found with name " + effectName);
	}

	public static void PlayEffect (string effectName, Vector3 newPosition, int colorID)
	{
		foreach (ParticleSystem effect in instance.particleList) {
			if (effect.name == effectName) {
				GameObject particleEffect = (GameObject)Instantiate (effect.gameObject);
				particleEffect.transform.SetParent (instance.transform);
				particleEffect.GetComponent<ParticleSystem> ().Stop ();
				particleEffect.transform.position = newPosition;

				particleEffect.GetComponent<ParticleSystemRenderer> ().material.SetColor ("_TintColor", GameManager.ColorList.GetColorList () [colorID]);
				particleEffect.transform.Find ("Buff").GetComponent<ParticleSystemRenderer> ().material.SetColor ("_TintColor", GameManager.ColorList.GetColorList () [colorID]);
				particleEffect.GetComponent<ParticleSystem> ().startColor = GameManager.ColorList.GetColorList () [colorID];
				//				effect.GetComponent<FXQ_MoveParticle> ().IsActive = true;
				particleEffect.GetComponent<ParticleSystem> ().Play ();
				SoundManager.PlaySfx2 (SoundManager.bubbleAudio);
				Destroy (particleEffect, 3f);
				return;
			}
		}

		Debug.LogWarning ("No effect found with name " + effectName);
	}
}
