﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PopUpMenu : MenuScreen
{

	// PopUp, Lock Sign anad shopping cart objects



	public Action<int> OnConfirmationClicked { get; set; }

	public static PopUpMenu instance;
	private GameObject popUpParent;

	private GameObject popUpForPurchase;
	private GameObject popUpForQuit;
	private GameObject popUpNotEnoughFunds;
	private GameObject popUpForReward;
	private GameObject basket;
	private GameObject onlyOneItem;

	// Popup texts
	private Text coinsToUnlockText;
	private Text explanationText;
	private Text questionText;
	private string shopingCartText;
	private Text popUpTotalPrice;
	//	private int levelInt;

	//	private int prevState;
	private MenuSystem menuSystem;

	private int purchasedItemType;

	private bool isPopUpActive;
	

	// 0 => VehicleList Manager, 1 => Level Manager, 2 => Customization Manager
	private int menuType;

	public bool IsPopUpActive {
		get {
			return isPopUpActive;
		}
	}

	//	private bool loaded = false;

	void Awake ()
	{
		instance = this;
		Initialization ();
//		loaded = true;
//		prevState = 0;
		base.Awake ();
		menuSystem = GetComponentInParent<MenuSystem> ();
	}

	public void QuitPopUpApprove ()
	{
		Application.Quit (); 
	}

	public override void Show (float fade, TransitionDirection direction, SimpleTween.Callback callback)
	{
		//	SoundManager.PlaySfx (SoundManager.buttonPressedAudio);
		base.Show (fade, direction, callback);
	}

	public void Hide ()
	{
		SoundManager.PlaySfx (SoundManager.buttonPressedAudio);
		CancelPopUp ();
		base.Hide (0.3f);
		isPopUpActive = false;
	}

	public void ActivateQuitPopUp ()
	{
		menuSystem.PopUpMenu.Show (0.3f);
		popUpForQuit.SetActive (true);
		isPopUpActive = true;
	}

	//	public void ActivateRewardPopUp ()
	//	{
	//		menuSystem.PopUpMenu.Show (0.3f);
	//		popUpForReward.SetActive (true);
	//		isPopUpActive = true;
	//	}

	public void QuitRewardPopUp ()
	{
		Hide ();
		//SaveManager.AddCoins (30000);
		//
	}

	void Initialization ()
	{
		OnConfirmationClicked = null;

//		popUpForPurchase = transform.Find ("PopUpPurchase").gameObject;
//		popUpNotEnoughFunds = transform.Find ("PopUpInsufficientCoins").gameObject;
		popUpForQuit = transform.Find ("PopUpForQuit").gameObject;
//		popUpForReward = transform.Find ("PopUpReward").gameObject;
		
//		onlyOneItem = popUpForPurchase.transform.FindChild ("PopUpFrame/OnlyOneItem").gameObject;
//		coinsToUnlockText = onlyOneItem.transform.Find ("OnlyOneItem2/MoneyContainer/CoinsToUnlockText").GetComponent<Text> ();
//		explanationText = onlyOneItem.transform.Find ("OnlyOneItem2/Image/ItemText").GetComponent<Text> ();
//		questionText = popUpForPurchase.transform.FindChild ("PopUpFrame/OnlyOneItem/QuestionText").gameObject.GetComponent<Text> ();
//		
//		basket = popUpForPurchase.transform.FindChild ("PopUpFrame/Basket").gameObject;
//		popUpTotalPrice = basket.transform.FindChild ("PopUpTotalMoneyContainer/CoinsToUnlockText").GetComponent<Text> ();

//		questionText.text = LocalizationManager2.instance.GetWord(questionText.text);

		isPopUpActive = false;
		instance.menuType = 0;
		instance.purchasedItemType = -1;
	}

	//	public void OnConfirmation ()
	//	{
	//		if (instance.OnConfirmationClicked != null)
	//			instance.OnConfirmationClicked (instance.purchasedItemType);
	//
	//	}

	// Deactivate all pop-ups
	public void CancelPopUp ()
	{
		try {
			//popUpForPurchase.SetActive (false);
			//popUpNotEnoughFunds.SetActive (false);
			popUpForQuit.SetActive (false);
			//popUpForReward.SetActive (false);
			//GameManager.Instance.State = prevState;
		} catch (System.Exception e) {
			Debug.Log (e);

		}

	}



	//	// Buy Action when clicked on Buy
	//	public void BuyAction (int price, string item, int purchasedItemType)
	//	{
	//		instance.SetPurchasedItemType (purchasedItemType);
	//		menuSystem.PopUpMenu.Show (0.3f);
	//		isPopUpActive = true;
	//
	//		if (SaveManager.HasSufficientCoins (price)) {
	//
	//			popUpForPurchase.SetActive (true);
	//			coinsToUnlockText.text = string.Format ("{0:0,0}", price);
	//			explanationText.text = item;
	////			explanationText.text = "You can " + item + " for :";
	//		} else {
	//			popUpNotEnoughFunds.SetActive (true);
	//		}
	//	}

	//	void SetPurchasedItemType (int purchasedItemType)
	//	{
	//		if (purchasedItemType == 3) {
	//			instance.basket.SetActive (true);
	//			instance.onlyOneItem.SetActive (false);
	//		} else {
	//			instance.basket.SetActive (false);
	//			instance.onlyOneItem.SetActive (true);
	//		}
	//
	//		instance.purchasedItemType = purchasedItemType;
	//	}
	//
	//
	//	public void SetVehicleNameForPopUp (string name)
	//	{
	//		basket.transform.Find ("TopInfoContainer/VehicleContainer/HeaderText").GetComponent<Text> ().text = name;
	//
	//	}
	//
	//	// Set shopping basket text
	//	public void SetShoppingBasketText (int[] itemPrices, int vehiclePrice, int totalPrice)
	//	{
	//		SetPurchasedItemType (3);
	//
	//		menuSystem.PopUpMenu.Show (0.3f);
	//		isPopUpActive = true;
	//
	//		//prevState = GameManager.Instance.State;
	//		if (SaveManager.HasSufficientCoins (totalPrice)) {
	//			popUpForPurchase.SetActive (true);
	//
	//			Transform priceContainer = basket.transform.Find ("TopInfoContainer").transform;
	//
	//
	//			if (vehiclePrice > 0) {
	//				Transform priceTextParent = priceContainer.GetChild (0);
	//				priceTextParent.Find ("PriceText").GetComponent<Text> ().text = string.Format ("{0:0,0}", vehiclePrice);
	//				priceTextParent.gameObject.SetActive (true);
	//			} else {
	//				priceContainer.GetChild (0).gameObject.SetActive (false);
	//			}
	//
	//			for (int i = 1; i < priceContainer.childCount; i++) {
	//				if (itemPrices [i - 1] > 0) {
	//					Transform priceTextParent = priceContainer.GetChild (i);
	//					priceTextParent.Find ("PriceText").GetComponent<Text> ().text = string.Format ("{0:0,0}", itemPrices [i - 1]);
	//					priceTextParent.gameObject.SetActive (true);
	//				} else {
	//					priceContainer.GetChild (i).gameObject.SetActive (false);
	//				}
	//			}
	//			popUpTotalPrice.text = string.Format ("{0:0,0}", totalPrice);
	//		} else
	//			popUpNotEnoughFunds.SetActive (true);
	//
	//		//GameManager.Instance.State = 6;
	//	}
	//

	// MenuType Getter/Setter
	public int MenuType {
		get {
			return menuType;
		}
		set {
			menuType = value;
			if (menuType == 3) {
				//instance.basket.SetActive (true);
				//instance.onlyOneItem.SetActive (false);
			} else {
				//instance.basket.SetActive (false);
				//instance.onlyOneItem.SetActive (true);
			}
		}
	}
}
