﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SimpleTween;
using UnityEngine.SceneManagement;

public class RestartMenu : MenuScreen
{
	private Transform scoreDetailArea;
	private Transform scoreArea;

	private Text scoreText;
	private Text bestScoreText;
	private Text coinText;
	private Transform newTextField;
	//	private Scene currentScene;
	private int coinValue;
	private GameObject bottomMenu;
	private float score;


	private GameObject noMovesLeft;


	private Text levelNameText;

	protected override void Awake ()
	{
		base.Awake ();

		newTextField = transform.FindChild ("NewTextField");

//		currentScene = SceneManager.GetActiveScene ();
//		print ("currentSceneName " + currentScene.name);
		newTextField.gameObject.SetActive (false);
		bestScoreText = transform.Find ("BestScoreContainer/BestScoreText").GetComponent<Text> ();
		scoreText = transform.Find ("CurrentScore").GetComponent<Text> ();

		noMovesLeft = transform.Find ("NoMovesLeft").gameObject;
		noMovesLeft.SetActive (false);
	}

	void OnDisable ()
	{
		newTextField.gameObject.SetActive (false);
	}



	public override void Show (float fade, TransitionDirection direction, SimpleTween.Callback callback)
	{

		base.Show (fade, direction, callback);
		ScreenShotTaker.ShowScreenShot ();
		HammurabiAds.ShowInterstitialAd (100);

	}




	//	public void WatchVideoToDoubleCoins ()
	//	{
	//		HammurabiUnityAds.Instance.ShowRewardedAd (1);
	//
	//	}



	//	public void OnWatchVideoCompleted ()
	//	{
	//		//HammurabiAnalytics.Instance.LogEvent ("Video Coins", GameManager.LevelName + "-" + GameManager.Player.VehicleModel.name, "Earned Coin", coinValue);
	//		//SaveManager.AddCoins (coinValue);
	//		doubleCoinsButton.SetActive (false);
	//		doubleCoinsCompleted.SetActive (true);
	//		SimpleTweener.AddTween (() => coinValue, x => coinText.text = Mathf.CeilToInt (x).ToString (), coinValue * 2.0f, 0.5f).Delay (0.8f).OnCompleted (() => {
	//			coinText.text = string.Format ("{0:0,0}", coinValue * 2);
	//
	//		});
	//
	//	}

	//	public void SetScore(float score){
	//		scoreText.text = Mathf.CeilToInt (score).ToString ();
	//	}


	public void SetHighestScore (float score)
	{
		scoreText.text = Mathf.CeilToInt (score).ToString ();
//		print (SaveManager.GetLevelScore ("Level01"));
//		print (score);
		if (score > SaveManager.GetLevelScore ("Level01")) {

			SaveManager.SetLevelScore ("Level01", score); 
			//			inGameMenuManager.PostScoreToLeaderBoard(score);
			HammurabiGameServices.Instance.PushScoreToLeaderBoard ("Level01", score);
			//bestScoreText.text = string.Format ("{0:0,0}", score);



			//SimpleTweener.AddTween (() => 0, x => bestScoreText.text = Mathf.CeilToInt (x).ToString (), score, 0.5f).Delay (1.5f);
			SoundManager.PlaySfx (SoundManager.winAudio);
			bestScoreText.text = Mathf.CeilToInt (score).ToString ();
			noMovesLeft.SetActive (false);
			newTextField.gameObject.SetActive (true);


//			if (GameManager.gameMode == GameManager.GameMode.TimeAttack) {
//				HammurabiGameServices.Instance.PushScoreToLeaderBoard (currentScene.name.ToString () + "TimeAttack", score);
//				SaveManager.SetLevelScore (currentScene.name.ToString () + "TimeAttack");
//			} else if (GameManager.gameMode == GameManager.GameMode.EndlessTwoWay) {
//				HammurabiGameServices.Instance.PushScoreToLeaderBoard (currentScene.name.ToString () + "TwoWay", score);
//				SaveManager.SetLevelScore (currentScene.name.ToString () + "TwoWay");
//			} else {
//				HammurabiGameServices.Instance.PushScoreToLeaderBoard (currentScene.name.ToString (), score);
//				SaveManager.SetLevelScore (currentScene.name.ToString ());
//			}

		} else {
			//SimpleTweener.AddTween (() => 0, x => bestScoreText.text = Mathf.CeilToInt (x).ToString (), oldHighestScore, 0.5f).Delay (1.5f);

			noMovesLeft.SetActive (true);
			bestScoreText.text = Mathf.CeilToInt (SaveManager.GetLevelScore ("Level01")).ToString ();
			SoundManager.PlaySfx (SoundManager.endOfTheGame);

			//GooglePlayServices.Instance.PushScoreToLeaderBoard (Application.loadedLevelName.ToString (), oldHighestScore);
		}
	}

	private void KickItem (Transform item, float amount, float time)
	{
		// animate the scale of an item to give it a little 'kick'
		SimpleTweener.AddTween (() => item.localScale, x => item.localScale = x, amount * item.localScale, time).Ease (Easing.EaseKick);
	}
}
