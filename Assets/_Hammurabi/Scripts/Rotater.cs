﻿using UnityEngine;
using System.Collections;

public class Rotater : MonoBehaviour
{
	
	private int sign;

	void OnEnable ()
	{
		if (Random.Range (0, 2) == 0)
			sign = -1;
		else
			sign = 1;
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Rotate (0, 0, sign * 200f * Time.deltaTime);
	}
}
