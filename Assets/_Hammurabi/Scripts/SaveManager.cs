using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using I2.Loc;

public class SaveManager : MonoBehaviour
{


	// LeaderBoardIds
	//	private static Dictionary<string, string> leaderBoardIdDic;

	// Does Score Saved for the levels
	//	private static Dictionary<string, int> doesScoreSavedDic;

	// Best Score For the Level
	//	private static ObscuredFloat levelHighestScore;

	// Count of the Coins
	private static ObscuredInt coinCount;


	//	public static SaveManager instance;
	//
	//	public static SaveManager Instance { get { return instance; } }

	// Initialization
	void Awake ()
	{
		// Target frame rate
		//Application.targetFrameRate = 200;

//		// LeaderBoard Ids
//		leaderBoardIdDic = new Dictionary<string, string> ();
//		leaderBoardIdDic.Add ("Level01", "ID1");
//		leaderBoardIdDic.Add ("Level02", "ID2");
//		leaderBoardIdDic.Add ("Level03", "ID3");
////		leaderBoardIdDic.Add ("Level04", "ID4");
////		leaderBoardIdDic.Add ("Level05", "ID5");
////		leaderBoardIdDic.Add ("Level06", "ID6");
//
//		// Does the highest scores are saved?
//		doesScoreSavedDic = new Dictionary<string, int> ();
//		doesScoreSavedDic.Add ("Level01Saved", -1);
//		doesScoreSavedDic.Add ("Level02Saved", -1);
//		doesScoreSavedDic.Add ("Level03Saved", -1);
//		doesScoreSavedDic.Add ("Level04Saved", -1);
//		doesScoreSavedDic.Add ("Level05Saved", -1);
//		doesScoreSavedDic.Add ("Level06Saved", -1);


		#region Game Started For The First Time 

		if (!ObscuredPrefs.HasKey ("RacingInCity_1")) {
			ObscuredPrefs.SetInt ("RacingInCity_1", 1);
			SetToggleSetting ("ColorBlindToggleGroup", 1);
			SetLanguageAtTheBeginning ();

//			print ("burda");
//			ObscuredPrefs.SetInt ("ColorBlindToggleGroup", 1);
//			ObscuredPrefs.Save ();
//							ObscuredPrefs.SetInt ("Music", 1);
//							ObscuredPrefs.SetInt ("AudioEffect", 1);
//							ObscuredPrefs.SetInt ("Coin", 7500);
//							ObscuredPrefs.SetInt ("Level01Unlocked", 1);
//							//			ObscuredPrefs.SetInt ("Level02Unlocked", 1);
//							//			ObscuredPrefs.SetInt ("Level03Unlocked", 1);
//							ObscuredPrefs.SetInt ("Vehicle00Unlocked", 1);
//							//			ObscuredPrefs.SetInt ("Vehicle01Unlocked", 1);
//							ObscuredPrefs.SetInt ("VehicleControl", 0); //button
//							ObscuredPrefs.SetBool ("CameraOption1", true);
//							ObscuredPrefs.SetBool ("CameraOption2", false);
//							ObscuredPrefs.SetBool ("AutoSpeed", false);
//							ObscuredPrefs.SetInt ("Quality", 1);
//							ObscuredPrefs.SetInt ("RemoveAds", 0);
////						ObscuredPrefs.SetInt ("StartCount", 0);
//							ObscuredPrefs.SetBool ("TwitterFollow", false);
//							ObscuredPrefs.SetBool ("FacebookLike", false);
//							ObscuredPrefs.SetBool ("RateUs", false);
//							SaveCustomizationItems_V1 ();
		} else {
			//			print("Eng");
			LocalizationManager.CurrentLanguage = SaveManager.GetLanguageSetting ();
		}

//		if(!ObscuredPrefs.HasKey ("LanguageOption")){
//			
//		}
//
		#endregion


		//LoadVehicleControlSetting ();

		// Get Coin Count
//		coinCount = GetCoinCount ();

//		HammurabiAnalytics.Instance.LogEvent ("TotalMoney" , "Money", "Money", coinCount);
	}

	void SetLanguageAtTheBeginning ()
	{
		if (Application.systemLanguage == SystemLanguage.Turkish && LocalizationManager.HasLanguage ("Turkish")) {
			LocalizationManager.CurrentLanguage = "Turkish";
			SaveManager.SetLanguageSetting ("Turkish");
		}
//		else if(Application.systemLanguage == SystemLanguage.German && LocalizationManager.HasLanguage("German")){
//			LocalizationManager.CurrentLanguage = "German";
//			SaveManager.SetLanguageSetting("German");
//		}else if(Application.systemLanguage == SystemLanguage.Portuguese && LocalizationManager.HasLanguage("Portuguese")){
//			LocalizationManager.CurrentLanguage = "Portuguese";
//			SaveManager.SetLanguageSetting("Portuguese");
//		}
		else {
			LocalizationManager.CurrentLanguage = "English";
			SaveManager.SetLanguageSetting ("English");
		}
	}

	// If there exist non-pushed scores, push them
	//	void Start ()
	//	{
	//		// Load does score saved data
	//		LoadDoesScoreSavedData ();
	//
	//		// For each Level
	//		for (int i = 1; i < doesScoreSavedDic.Count; i++) {
	//			string levelName = "Level" + string.Format ("{0:00}", i);
	//			string isSavedTemp = levelName + "Saved";
	//
	//			// Get and push the scores
	//			if (doesScoreSavedDic [isSavedTemp] == 0) {
	//				Social.ReportScore ((long)GetLevelScore (levelName), leaderBoardIdDic [levelName], (bool success) => {
	//					if (success == true)
	//						doesScoreSavedDic [isSavedTemp] = 1;
	//				});
	//			}
	//		}
	//	}

	//	// Load doesScoreSavedDic data from playerpref
	//	public static void LoadDoesScoreSavedData ()
	//	{
	//		//If doesScoreSavedDic element is equal to 0, score is not saved b4
	//		//If doesScoreSavedDic element is equal to -1, player is not played that level
	//		//If doesScoreSavedDic element is equal to 1, score is saved b4
	//		for (int i = 1; i < doesScoreSavedDic.Count; i++) {
	//			string temp = "Level" + string.Format ("{0:00}", i) + "Saved";
	//
	//			if (ObscuredPrefs.HasKey (temp)) {
	//				doesScoreSavedDic [temp] = ObscuredPrefs.GetInt (temp);
	//			} else {
	//				doesScoreSavedDic [temp] = -1;
	//			}
	//		}
	//	}

	// Set level Score
	public static void SetLevelScore (string levelName, float highestScore)
	{
		ObscuredPrefs.SetFloat (levelName + "Score", highestScore);

		ObscuredPrefs.Save ();
	}

	// Get level Score
	public static float GetLevelScore (string levelName)
	{
		levelName = levelName + "Score";

		return ObscuredPrefs.GetFloat (levelName);
	}

	//	public static string GetLeaderBoardId (string levelName)
	//	{
	//		return leaderBoardIdDic [levelName];
	//
	//	}

	//Increase Coin Amount
	//	public static void AddCoins (int coinsToAdd)
	//	{
	//		AddCoinsWithoutAnimation_1 (coinsToAdd);
	//		PlayCoinAnimationAndSetText_2 ();
	//	}

	//	public static void AddCoinsWithoutAnimation_1 (int coinsToAdd)
	//	{
	//		coinCount += coinsToAdd;
	//		SetCoinCount ();
	//	}

	//	public static void PlayCoinAnimationAndSetText_2 ()
	//	{
	//		if (TopMenuManager.Instance != null) {
	//			TopMenuManager.Instance.SetCoinText ();
	//		}
	//	}

	//	public static void DecreaseCoins (int coinsToDecrease)
	//	{
	//		if (coinsToDecrease <= coinCount) {
	//			AddCoins (-coinsToDecrease);
	//		} else {
	//			// Not Enough Funds
	////			PopUpMenu.instance
	//		}
	//	}

	// Set Coin Amount
	public static void SetCoinCount ()
	{
		ObscuredPrefs.SetInt ("Coin", coinCount);
		ObscuredPrefs.Save ();
	}

	// Get Coin Amount
	public static int GetCoinCount ()
	{
//		print ("Coin Get");
		return ObscuredPrefs.GetInt ("Coin");

		
	}


	public static void SetToggleSetting (string toggleName, int toggleOption)
	{
		ObscuredPrefs.SetInt (toggleName, toggleOption);
		ObscuredPrefs.Save ();
	}

	// Get Coin Amount
	public static int GetToggleSetting (string toggleName)
	{
//		print (toggleName + " - " + ObscuredPrefs.GetInt (toggleName));
		return ObscuredPrefs.GetInt (toggleName);


	}

	//	public static void SetColorListOption (bool colorListOption)
	//	{
	//		ObscuredPrefs.SetBool ("ColorListOption",colorListOption);
	//		ObscuredPrefs.Save ();
	//	}
	//
	//	// Get Coin Amount
	//	public static bool GetColorListOption ()
	//	{
	//		//		print ("Coin Get");
	//		return ObscuredPrefs.GetBool ("ColorListOption");
	//
	//
	//	}


	// Set Coin Amount
	public static void SetTotalDistance (string levelName, int distance)
	{
		ObscuredPrefs.SetInt ("TotalDistance" + levelName, distance);
		ObscuredPrefs.Save ();
	}

	// Get Coin Amount
	public static int GetTotalDistance (string levelName)
	{
		//		print ("Coin Get");
		return ObscuredPrefs.GetInt ("TotalDistance" + levelName);


	}

	//	// Set Coin Amount
	//	public static void SetSpeedometerSetting (int speedometer)
	//	{
	//		ObscuredPrefs.SetInt ("SpeedometerSetting", speedometer);
	//		ObscuredPrefs.Save ();
	//	}
	//
	//	// Get Coin Amount
	//	public static int GetSpeedometerSetting ()
	//	{
	//		//		print ("Coin Get");
	//		return ObscuredPrefs.GetInt ("SpeedometerSetting");
	//
	//
	//	}

	// Set Coin Amount
	public static void SetLanguageSetting (string lang)
	{
		ObscuredPrefs.SetString ("LanguageOption", lang);
		ObscuredPrefs.Save ();
	}

	// Get Coin Amount
	public static string GetLanguageSetting ()
	{
		//		print ("Coin Get");
		return ObscuredPrefs.GetString ("LanguageOption");


	}

	// Set Coin Amount
	public static void SetDateTime (long dateTime)
	{
		ObscuredPrefs.SetLong ("DateTime", dateTime);
		ObscuredPrefs.Save ();
	}

	// Get Coin Amount
	public static long GetDateTime ()
	{
		//		print ("Coin Get");
		return ObscuredPrefs.GetLong ("DateTime");


	}


	public static int GetCompletedGameCount ()
	{
		return ObscuredPrefs.GetInt ("CompletedGame");
	}

	public static void IncreaseCompletedGameCount ()
	{
//		if (GetStartCount () < 30) {
		ObscuredPrefs.SetInt ("CompletedGame", GetCompletedGameCount () + 1);
		ObscuredPrefs.Save ();
//		}
	}

	public static void IncreaseStartCount ()
	{
		//		if (GetStartCount () < 30) {
		ObscuredPrefs.SetInt ("StartCount", GetStartCount () + 1);
		ObscuredPrefs.Save ();
		//		}
	}

	public static int GetStartCount ()
	{
		return ObscuredPrefs.GetInt ("StartCount");
	}

	// Set Coin Amount
	public static void SetBonusIndex (int bonusIndex)
	{
		ObscuredPrefs.SetInt ("BonusIndex", bonusIndex);
		ObscuredPrefs.Save ();
	}

	// Get Coin Amount
	public static int GetBonusIndex ()
	{
		//		print ("Coin Get");
		return ObscuredPrefs.GetInt ("BonusIndex");


	}



	// Save Lastly Played Car
	public static void SaveLastPlayedVehicle (int index)
	{
		ObscuredPrefs.SetInt ("LastPlayedCar", index);
		ObscuredPrefs.Save ();
	}

	// Get Lastly Played Car
	public static int GetLastPlayedCar ()
	{
		return ObscuredPrefs.GetInt ("LastPlayedCar");
		
	}

	public static void SaveLastPlayedLevel (int index)
	{
		ObscuredPrefs.SetInt ("LastPlayedLevel", index);
		ObscuredPrefs.Save ();
	}

	public static int GetLastPlayedLevel ()
	{
		return ObscuredPrefs.GetInt ("LastPlayedLevel");
		
	}

	// Set Remove Ads Pref
	public static void SetRemoveAds (int condition)
	{
		ObscuredPrefs.SetInt ("RemoveAds", condition);
		ObscuredPrefs.Save ();
	}

	// Get Remove Ads Pref
	public static bool GetRemoveAds ()
	{
		if (ObscuredPrefs.GetInt ("RemoveAds") == 1) {
			return true;
		} else {
			return false;
		}
	}



	// Set Facebook Like Pref
	public static void SetFacebookLike (bool condition)
	{
		ObscuredPrefs.SetBool ("SetFacebookLike", condition);
		ObscuredPrefs.Save ();
	}

	// Get Facebook Like Pref
	public static bool GetFacebookLike ()
	{
		return ObscuredPrefs.GetBool ("SetFacebookLike");

	}

	// Set Facebook Like Pref
	public static void SetGiveReward (bool condition)
	{
		ObscuredPrefs.SetBool ("GiveReward", condition);
		ObscuredPrefs.Save ();
	}

	// Get Facebook Like Pref
	public static bool GetGiveReward ()
	{
		return ObscuredPrefs.GetBool ("GiveReward");

	}

	// Set Facebook Like Pref
	public static void SetRateUs (bool condition)
	{
		ObscuredPrefs.SetBool ("RateUs", condition);
		ObscuredPrefs.Save ();
	}

	// Get Facebook Like Pref
	public static bool GetRateUs ()
	{
		return ObscuredPrefs.GetBool ("RateUs");
	}

	public static void DeletePlayerPrefs ()
	{

		ObscuredPrefs.DeleteAll ();
//		print (GetToggleSetting ("ColorBlindToggleGroup"));
	}

	// Set Facebook Like Pref
	public static void SetTwitterFollow (bool condition)
	{
		ObscuredPrefs.SetBool ("TwitterFollow", condition);
		ObscuredPrefs.Save ();
	}

	// Get Facebook Like Pref
	public static bool GetTwitterFollow ()
	{
		return ObscuredPrefs.GetBool ("TwitterFollow");
	}

	#region Settings Menu - START----------------------------------------

	// Set Audio Effect Setting
	public static void SaveAudioEffectVolume (float value)
	{
		ObscuredPrefs.SetFloat ("AudioEffectVol", value);
		ObscuredPrefs.Save ();
	}

	// Set Music Setting
	public static void SaveAudioMusicVolume (float value)
	{
		//print ("SES DEGISTI " + value);
		ObscuredPrefs.SetFloat ("MusicVol", value);
		ObscuredPrefs.Save ();
	}

	// Get Audio Effect Setting
	public static float GetAudioEffectVolume ()
	{
		if (ObscuredPrefs.HasKey ("AudioEffectVol"))
			return ObscuredPrefs.GetFloat ("AudioEffectVol");
		else
			return 0;
	}

	// Get Music Setting
	public static float GetMusicVolume ()
	{
//		print(ObscuredPrefs.HasKey ("MusicVol"));
		if (ObscuredPrefs.HasKey ("MusicVol"))
			return ObscuredPrefs.GetFloat ("MusicVol");
		else
			return 0;
	}
		

	// Set Quality Settings
	public static void SaveQualitySetting (int qualityLevel)
	{
		ObscuredPrefs.SetInt ("Quality", qualityLevel);
		ObscuredPrefs.Save ();
	}

	// Get Quality Settings
	public static int GetQualitySetting ()
	{
		return ObscuredPrefs.GetInt ("Quality");
	}

	// Save Vehicle Control Settings
	public static void SaveVehicleControlSetting (int index)
	{
		ObscuredPrefs.SetInt ("VehicleControl", index);
		ObscuredPrefs.Save ();
	}

	public static void SaveCameraOptions (int id, bool index)
	{
		if (id == 0) {
			ObscuredPrefs.SetBool ("CameraOption1", index);
		} else {
			ObscuredPrefs.SetBool ("CameraOption2", index);
		}
//		ObscuredPrefs.SetBool ("CameraOption2", index2);
		ObscuredPrefs.Save ();
	}

	public static bool GetCameraOption (int type)
	{
		if (type == 0) {
			return ObscuredPrefs.GetBool ("CameraOption1");
		} else {
			return ObscuredPrefs.GetBool ("CameraOption2");
		}

	}

	public static void SaveAutoSpeedOption (bool condition)
	{

		ObscuredPrefs.SetBool ("AutoSpeed", condition);
		
		//		ObscuredPrefs.SetBool ("CameraOption2", index2);
		ObscuredPrefs.Save ();
	}

	public static bool GetAutoSpeedOption ()
	{
		return ObscuredPrefs.GetBool ("AutoSpeed");
	}

	//	// Load vehicle control settings
	//	public static void LoadVehicleControlSetting ()
	//	{
	//
	//		// Get previous Vehicle control settings
	//		if (ObscuredPrefs.HasKey ("VehicleControl")) {
	//			GameManager.vehicleControlOption = ObscuredPrefs.GetInt ("VehicleControl");
	//		}
	//	}

	// Get Vehicle Control Settings
	public static int GetVehicleControlSetting ()
	{
		return ObscuredPrefs.GetInt ("VehicleControl");
	}

	#endregion

	// Save the unlock Value of the item
	public static void SaveUnlockValue (string itemName, int value)
	{
		string unlockKey = itemName + "Unlocked";
		ObscuredPrefs.SetInt (unlockKey, value);
		ObscuredPrefs.Save ();
	}

	// Get the unlock Value of the item
	public static int GetUnlockValue (string itemName)
	{
		string temp = itemName + "Unlocked";

		return ObscuredPrefs.GetInt (temp);
	}



	#region Car Modify Menu - START----------------------------------------

	public static void SaveUnlockedItemCountForVehicle (string itemName, int itemType)
	{
		string previouslySaved = ObscuredPrefs.GetString (itemName);

//		print (previouslySaved);
		List<string> itemListStr = new List<string> (previouslySaved.Split (';'));
		List<int> itemList = new List<int> ();

//		print (itemListStr[0]);
		foreach (string item in itemListStr) {
			if (!string.IsNullOrEmpty (item))
				itemList.Add (System.Int32.Parse (item));
		}

		foreach (int itemTemp in itemList) {
			if (itemType != itemTemp) {
			} else {
				return;
			}
		}

		previouslySaved += ";" + itemType.ToString ();
		ObscuredPrefs.SetString (itemName, previouslySaved);
	}

	// Save the color
	public static void SaveCustomizationItem (string itemName, int custItemId, int selectedItemIndex)
	{
//		string temp = itemName + "Customization%+" + custItemId.ToString ();
//		print (temp);
		ObscuredPrefs.SetInt (itemName + "Customization%+" + custItemId.ToString (), selectedItemIndex);
		ObscuredPrefs.Save ();
	}

	void SaveCustomizationItemWithoutSave (string itemName, int custItemId, int selectedItemIndex)
	{
		ObscuredPrefs.SetInt (itemName + "Customization%+" + custItemId.ToString (), selectedItemIndex);
//		ObscuredPrefs.Save ();
	}



	// Get the color
	public static int GetCustomizationItem (string itemName, int custItemId)
	{
		string temp = itemName + "Customization%+" + custItemId.ToString ();
//		print (temp);
		return ObscuredPrefs.GetInt (temp);
		
	}

	#endregion


	//	public static ObscuredFloat LevelHighestScore {
	//		get {
	//			return levelHighestScore;
	//		}
	//		set {
	//			levelHighestScore = value;
	//		}
	//	}

	public static ObscuredInt CoinCount {
		get {
			return coinCount;
		}
		set {
			coinCount = value;
		}
	}

	// If player has enough coins, true
	public static  bool HasSufficientCoins (int neededCoins)
	{
		if (coinCount >= neededCoins) {
			return true;
		} else {
			return false;
		}
		
	}

}