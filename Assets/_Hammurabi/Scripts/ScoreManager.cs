﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleTween;

public class ScoreManager : MonoBehaviour
{

	//	public float closeCallComboDuration = 3.1f;
	//	private int currentCloseCallScore;
	//	private float speedOfTheVehicle;
	//	private float distance;
	//	private float highSpeedStartDistance;
	//	private float wrongWayStartDistance;
	//	private int highSpeedScore;
	//	private int wrongWayScore;
	//	private int closeCallScore;
	//	private int closeCallCombo;
	//	private bool comboAvailable;
	//	private int numberOfCloseCalls;
	//	private float highSpeedDuration;
	//	private float wrongWayDuration;

	private int totalScore;
	private int scoreMult;
	private int scoreOfTheMove;

//	public void ResetMoveValues(){
//		scoreMult = 0;
//		scoreOfTheMove = 0;
//	}


	// Use this for initialization
	void Start ()
	{
		Reset ();
		GameManager.ScoreManager = this;
	}


	
//	// Update is called once per frame
//	void Update ()
//	{
//
//		if (GameManager.gameMode != GameManager.GameMode.FreeMode)
//			ScoreCalculation ();
//			
//	}
	public void Reset(){
		totalScore = 0;
		scoreMult = 0;
		scoreOfTheMove = 0;
	}

	public void ScoreCalculation ( Text currentScoreText)
	{
//		print (scoreMult +"-" + scoreOfTheMove);
		totalScore += scoreMult * scoreOfTheMove;
		currentScoreText.text = Mathf.CeilToInt (totalScore).ToString ();
	}

	public int ScoreMult {
		get {
			return scoreMult;
		}
		set {
			scoreMult = value;
		}
	}

	public int ScoreOfTheMove {
		get {
			return scoreOfTheMove;
		}
		set {
			scoreOfTheMove = value;
		}
	}

	public int TotalScore {
		get {
			return totalScore;
		}
		set {
			totalScore = value;
		}
	}



	//	// true => right, false =>left
	//	void CloseCallScoreCalculation (bool rightOrLeft)
	//	{
	//		if (!comboAvailable && closeCallCombo == -1) {
	//			closeCallCombo++;
	//			StopCoroutine ("CloseCallComboTime");
	//			StartCoroutine ("CloseCallComboTime");
	////			hud.StartComboPanel(closeCallCombo);
	//
	//		} else if (comboAvailable && closeCallCombo == 0) {
	//			closeCallCombo++;
	//			StopCoroutine ("CloseCallComboTime");
	//			StartCoroutine ("CloseCallComboTime");
	//			hud.StartComboPanel (closeCallCombo);
	//
	//		} else if (comboAvailable) {
	//			closeCallCombo++;
	//			StopCoroutine ("CloseCallComboTime");
	//			StartCoroutine ("CloseCallComboTime");
	//			hud.ShowComboPanel (closeCallCombo);
	//		}
	//		if (closeCallCombo <= 20) {
	//			currentCloseCallScore = closeCallCombo > 0 ? closeCallCombo * 10 : 10; // GameManager.Player.Speed >= 100 ? 40 : 20;
	//		} else if (closeCallCombo <= 50) {
	//			currentCloseCallScore = 200 + (closeCallCombo - 20) * 20;
	//		} else {
	//			currentCloseCallScore = 800;
	//		}
	//		hud.CloseCallJob (rightOrLeft, currentCloseCallScore);
	//
	////		numberOfCloseCalls++;
	//		closeCallScore += currentCloseCallScore;
	//		ChangeFinalScore ();
	//
	//	}
	//
	//	IEnumerator CloseCallComboTime ()
	//	{
	//		comboAvailable = true;
	//		yield return new WaitForSeconds (closeCallComboDuration * (1 / GameManager.Player.SpeedMultiplier));
	//		hud.CloseComboPanel ();
	//		comboAvailable = false;
	//		closeCallCombo = -1;
	//
	//	}
	//
	//
	//
	//
	//	public int CloseCallScore {
	//		get {
	//			return closeCallScore;
	//		}
	//		set {
	//			closeCallScore = value;
	//		}
	//	}



	//	public int NumberOfCloseCalls {
	//		get {
	//			return numberOfCloseCalls;
	//		}
	//		set {
	//			numberOfCloseCalls = value;
	//		}
	//	}


}