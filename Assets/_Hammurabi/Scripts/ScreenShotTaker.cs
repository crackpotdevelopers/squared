﻿using UnityEngine;
using System.Collections;
using AppAdvisory.SharingSystem;

public class ScreenShotTaker : MonoBehaviour
{
	public static ScreenShotTaker instance;

	void Awake ()
	{
		if (!instance) {
			instance = this;
		}
	}

	public static void TakeScreenShot ()
	{
		VSSHARE.DOTakeScreenShot ();
	}

	public static void ShowScreenShot ()
	{
		VSSHARE.DOOpenScreenshotButton ();
	}
}
