﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using I2.Loc;

public class SettingsController : MonoBehaviour
{
	private Transform settingsMenuParent;
	private HammurabiToggle[] togglesInSettings;

	private Dropdown langDropDown;

	GameObject[] whiteDots;

	private enum Languages
	{
		English = 0,
		Turkish = 1
	}

	void Awake ()
	{
		langDropDown = transform.Find ("SettingsMenu/Panel/SettingsButtonPanel/LanguageDropdown").GetComponent<Dropdown> ();
		settingsMenuParent = transform.Find ("SettingsMenu");

		whiteDots = new GameObject[9];
		whiteDots = GameObject.FindGameObjectsWithTag ("WhiteDots");
	}

	void Start ()
	{
		
		togglesInSettings = settingsMenuParent.GetChild (0).GetComponentsInChildren<HammurabiToggle> ();
		togglesInSettings [0].OnToggleOnOff += ChangeMusicSetting;
		togglesInSettings [1].OnToggleOnOff += ChangeColorBlindSettings;
		togglesInSettings [0].Init ();
		togglesInSettings [1].Init ();



		langDropDown.onValueChanged.AddListener (ChangeLanguage);
		langDropDown.value = (int)((Languages)System.Enum.Parse (typeof(Languages), SaveManager.GetLanguageSetting ()));
	}

	public void ChangeLanguage (int langID)
	{

		LocalizationManager.CurrentLanguage = ((Languages)langID).ToString ();

		SaveManager.SetLanguageSetting (LocalizationManager.CurrentLanguage);

	}

	void ChangeMusicSetting (int index)
	{
		
		AudioListener.pause = Convert.ToBoolean (index);
	}

	void ChangeColorBlindSettings (int index)
	{
		
		GameManager.ColorList.IsNormalListUsed = Convert.ToBoolean (index);
		MenuManager.Instance.OnChangeColors ();
		ChangeColorOfDots ();
			
	}

	void ChangeColorOfDots ()
	{
		if (!GameManager.ColorList.IsNormalListUsed)
			for (int i = 0; i < whiteDots.Length; i++) {
				whiteDots [i].GetComponent<Image> ().color = Color.grey;
				
			}
		else {
			for (int i = 0; i < whiteDots.Length; i++) {
				whiteDots [i].GetComponent<Image> ().color = Color.white;

			}
		}
	}


	//	void Start ()
	//	{
	//
	//	}

	//	private float currentSFXLevel;
	//	private float currentMusicLevel;
	//	private Slider musicSlider;
	//	private Slider sfxSlider;


	//	public void SetPreviousSettings ()
	//	{
	//		musicSlider = transform.Find ("SettingsMenu/Audio/AudioSettings/MusicSlider").GetComponent<Slider> ();
	//		sfxSlider = transform.Find ("SettingsMenu/Audio/AudioSettings/SoundFXSlider").GetComponent<Slider> ();
	//		currentMusicLevel = SaveManager.GetMusicVolume ();
	//		currentSFXLevel = SaveManager.GetAudioEffectVolume ();
	//		musicSlider.value = currentMusicLevel;
	//		sfxSlider.value = currentSFXLevel;
	//		SetMusicLevel (currentMusicLevel);
	//		SetSoundFXLevel (currentSFXLevel);
	//
	//	}
	//
	//
	//	IEnumerator WaitForSettings ()
	//	{
	//		yield return null;
	//		SetPreviousSettings ();
	//	}
	//
	//	public void SetMusicLevel (float soundLvl)
	//	{
	//		currentMusicLevel = soundLvl;
	//		SoundManager.SetMusicLvl (soundLvl);
	//	}
	//
	//	public void SetSoundFXLevel (float soundLvl)
	//	{
	//		currentSFXLevel = soundLvl;
	//		SoundManager.SetSfxLvl (soundLvl);
	//	}
	//
	//
	//	public void SaveSoundSettings ()
	//	{
	//		SoundManager.PlaySfx (SoundManager.buttonPressedAudio);
	//		SaveManager.SaveAudioMusicVolume (currentMusicLevel);
	//		SaveManager.SaveAudioEffectVolume (currentSFXLevel);
	////		print("CurMusic :" + currentMusicLevel + " - CurSFX :" + currentSFXLevel);
	//	}
}
