﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

/// <summary>
/// A simple sound manager class for handling the playing of sound effects and music
/// </summary>
public class SoundManager : MonoBehaviour
{
	// list of available sound clips
	public AudioClip[] sounds;
	// list of available music tracks
	public AudioClip[] music;


	private AudioSource sfxAudioSource;
	private AudioSource sfxAudioSource2;
	private AudioSource musicAudioSource;

	private AudioSource powerUpFXAudioSource;

	public AudioMixer masterMixer;
	public static SoundManager instance;

	public static string buttonPressedAudio = "ButtonPress";
	public static string dropAudio = "DropItem";
	public static string laserBeamPowerUpAudio = "LaserBeamPowerUp";
	public static string bubbleAudio = "Bubble";
	public static string dropNegativeAudio = "DropNegative";
	public static string winAudio = "Win";
	public static string endOfTheGame = "EndOfTheGame";


	public static string gameLoopAudio = "GameLoop";

	void Awake ()
	{
		if (!instance) {
			instance = this;
		}
			
		sfxAudioSource = transform.Find ("SoundFXPlayer").GetComponent<AudioSource> ();
		sfxAudioSource2 = transform.Find ("SoundFXPlayer2").GetComponent<AudioSource> ();
		musicAudioSource = transform.Find ("MusicPlayer").GetComponent<AudioSource> ();


	}


	/// <summary>
	/// Play a sound clip by name
	/// </summary>
	/// <param name="sfxName">The name of the sound to play</param>
	public static void PlaySfx (string sfxName)
	{
		if (instance == null) {
			Debug.LogWarning ("Attempt to play a sound with no SoundManager in the scene");
			return;
		}

		instance.PlaySound (sfxName, instance.sounds, instance.sfxAudioSource);
	}

	public static void PlaySfx2 (string sfxName)
	{
		if (instance == null) {
			Debug.LogWarning ("Attempt to play a sound with no SoundManager in the scene");
			return;
		}

		instance.PlaySound (sfxName, instance.sounds, instance.sfxAudioSource2);
	}


	public static void SetSfxLvl (float sfxLvl)
	{
		instance.masterMixer.SetFloat ("sfxVol", sfxLvl);
	}


	public static void SetMusicLvl (float musicLvl)
	{
		instance.masterMixer.SetFloat ("musicVol", musicLvl);
	}

	/// <summary>
	/// Plays a given sound clip	
	/// </summary>
	/// <param name="clip">The sound clip to play.</param>
	public static void PlaySfx (AudioClip clip)
	{
		instance.PlaySound (clip, instance.sfxAudioSource);
	}

	/// <summary>
	/// Start playing a music track from the beginning
	/// </summary>
	/// <param name="trackName">Track name.</param>
	public static void PlayMusic (string trackName)
	{
		if (instance == null) {
			Debug.LogWarning ("Attempt to play a sound with no SoundManager in the scene");
			return;
		}

		instance.PlaySound (trackName, instance.music, instance.musicAudioSource);
	}

	/// <summary>
	/// Pauses the music.
	/// </summary>
	/// <param name="fadeTime">Fade out time.</param>
	public static void PauseMusic (float fadeTime)
	{
		if (fadeTime > 0.0f)
			instance.StartCoroutine (instance.FadeMusicOut (fadeTime));
		else
			instance.musicAudioSource.Pause ();
	}

	/// <summary>
	/// Unpauses the music.
	/// </summary>
	public static void UnpauseMusic ()
	{

		//instance.musicAudioSource.volume = 1.0f;
		instance.musicAudioSource.Play ();
	}

	/// <summary>
	/// Plays a sound using a given AudioSource
	/// </summary>
	private void PlaySound (string soundName, AudioClip[] pool, AudioSource audioOut)
	{
		// loop through our list of clips until we find the right one.
		foreach (AudioClip clip in pool) {
			if (clip.name == soundName) {
				PlaySound (clip, audioOut);
				return;
			}
		}

		Debug.LogWarning ("No sound clip found with name " + soundName);
	}


	/// <summary>
	/// Plays a sound using a given AudioSource
	/// </summary>
	private void PlaySound (AudioClip clip, AudioSource audioOut)
	{
		audioOut.clip = clip;
		audioOut.Play ();
	}

	/// <summary>
	/// Co-Routine for fading out the music
	/// </summary>
	/// <param name="time">Fade time</param>
	IEnumerator FadeMusicOut (float time)
	{
		float startVol = musicAudioSource.volume;
		float startTime = Time.realtimeSinceStartup;

		while (true) {
			// use realtimeSinceStartup because Time.time doesn't increase when the game is paused.
			float t = (Time.realtimeSinceStartup - startTime) / time;
			if (t < 1.0f) {
				musicAudioSource.volume = (1.0f - t) * startVol;
				yield return 0;
			} else {
				break;
			}
		}

		// once we've fully faded out, pause the track
		musicAudioSource.Pause ();
	}
}
